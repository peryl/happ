// // Import and configure the Firebase SDK
// // These scripts are made available when the app is served or deployed on Firebase Hosting
// // If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup
// importScripts('/__/firebase/9.2.0/firebase-app-compat.js');
// importScripts('/__/firebase/9.2.0/firebase-messaging-compat.js');
// importScripts('/__/firebase/init.js');
// const messaging = firebase.messaging();

/**
 * Here is is the code snippet to initialize Firebase Messaging in the Service
 * Worker when your app is not hosted on Firebase Hosting.
 **/
// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/9.8.0/firebase-app-compat.js');
importScripts('https://www.gstatic.com/firebasejs/9.8.0/firebase-messaging-compat.js');

self.addEventListener('notificationclick', function (event) {
    console.log('SW notification click event', event);
    event.notification.close();
    // const url = event.notification.data.FCM_MSG.notification.click_action;
    const url = "/#" + event.notification.data.FCM_MSG.data.navigate;
    event.waitUntil(
        clients.matchAll({type: 'window'})
            .then(windowClients => {
                // Check if there is already a window/tab open with the target URL
                for (var i = 0; i < windowClients.length; i++) {
                    var client = windowClients[i];
                    // console.log(client.url, url);
                    // If so, just focus it.
                    // if (client.url === url && 'focus' in client) {
                    if (client.url.endsWidth(url) && 'focus' in client) {
                        return client.focus();
                    }
                }
                // If not, then open the target URL in a new window/tab.
                if (clients.openWindow) {
                    return clients.openWindow(url);
                }
            })
            .catch(console.error)
    );
})

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
    apiKey: "AIzaSyCnrSMsBbiDAdO9FUNWsrDaGpoWSsZL8vA",
    authDomain: "peryl-pwa.firebaseapp.com",
    projectId: "peryl-pwa",
    storageBucket: "peryl-pwa.appspot.com",
    messagingSenderId: "109207901144",
    appId: "1:109207901144:web:3d7f6ce04fee96790218ad"
});
// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();


// // If you would like to customize notifications that are received in the
// // background (Web app is closed or not in browser focus) then you should
// // implement this optional method.
// // Keep in mind that FCM will still show notification messages automatically
// // and you should use data messages for custom notifications.
// // For more info see:
// // https://firebase.google.com/docs/cloud-messaging/concept-options
// messaging.onBackgroundMessage(function(payload) {
//   console.log('[firebase-messaging-sw.js] Received background message ', payload);
// //   // Customize notification here
// //   const notificationTitle = 'Background Message Title';
// //   const notificationOptions = {
// //     body: 'Background Message body.',
// //     icon: '/firebase-logo.png'
// //   };
// //   self.registration.showNotification(notificationTitle, notificationOptions);
//   self.registration.showNotification("+++" + payload.notification.title, {
//     body: payload.notification.body,
//     icon: payload.notification.icon,
//     url: payload.notification.click_action,
//     // badge: "images/badge.png",`
//     // vibrate: [100, 50, 100],
//     // data: {
//     //     onAction: {
//     //         // "": location.href,
//     //         like: "http://bbb.sk",
//     //         reply: "http://ccc.sk"
//     //     }
//     // },
//     // actions: [
//     //     { action: "like", title: "Like" },
//     //     { action: "reply", title: "Reply" }
//     // ]
//   });
// });
