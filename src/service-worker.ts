import { manifest, version } from "@parcel/service-worker";

async function install() {
  const cache = await caches.open(version);
  await cache.addAll(manifest);
}
addEventListener("install", (e: any) => e.waitUntil(install()));

async function activate() {
  const keys = await caches.keys();
  await Promise.all(
    keys.map(key => key !== version && caches.delete(key))
  );
}
addEventListener("activate", (e: any) => e.waitUntil(activate()));


// Import Pushy Service Worker 1.0.9

// importScripts("https://sdk.pushy.me/web/1.0.9/pushy-service-worker.js");
import "./pushy-service-worker-1.0.9.js";
