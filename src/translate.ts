
export interface Translations {
    [message: string]: {
        [lang: string]: string;
    }
}

let ts: Translations = {};

let ls: string[] = [];

export function _(text: string, keyPrefix = ""): string {
    const key = keyPrefix ? `${keyPrefix}|${text}` : text;
    if (text in ts) {
        for (const lang of ls) {
            if (lang in ts[key]) {
                return ts[key][lang];
            }
        }
        return text;
    } else {
        ts[key] = { [ls[0]]: text };
        console.log(JSON.stringify(ts, null, 4));
        return text;
    }
}

export function translations(): Translations {
    return ts;
}

/**
 *
 * @param languages Usually "navigator.languages"
 * @param transpations Translations
 */
export function translateInit(
    languages: readonly string[],
    transpations: Translations
): void {
    ls = [...new Set(languages.map((l) => l.split("-")[0]))];
    if (!ls.length) {
        ls.push("en");
    }
    ts = transpations;
}
