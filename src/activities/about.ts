import { HAppI } from "peryl/dist/hsml-app";
import { HashUrlDataState } from "..";

export interface AboutState extends HashUrlDataState {
    title: string;
}

export const about: HAppI<AboutState, string> = {

    name: "about",

    state: function () {
        return {
            title: "About"
        }
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["p", ["Hello", " ", state.hashUrlData?.path[1] || "", " !"]],
                ["p", [
                    ["a", { href: "https://gitlab.com/peryl/happ" }, "This"],
                    " is ",
                    ["b", "mobile/web app technology rapid development demo "],
                    "using ",
                    ["b", [["a", { href: "https://gitlab.com/peryl" }, "PeRyL"]]],
                    " tools created by ", ["b", "Peter Rybar"], " ",
                    ["a",
                        {
                            href: "https://www.linkedin.com/in/peter-rybar/",
                            title: "LinkedIn"
                        },
                        [["i.fa.fa-fw.fa-linkedin-square"]]
                    ],
                    " ",
                    ["a",
                        {
                            href: "https://gitlab.com/peter-rybar",
                            title: "GitLab"
                        },
                        [["i.fa.fa-fw.fa-gitlab"]]
                    ],
                    "."
                ]],
                ["p", [
                    "The demo is intended for ", ["b", "startups and progressive projects "],
                    "that require ", ["b", "fast delivery"], " and need to ",
                    ["b", "reduce development costs"], "."
                ]],
                ["div.w3-panel.w3-leftbar.w3-white", [
                    ["p", [
                        ["i.fa.fa-quote-right.w3-xlarge"],
                        ["br"],
                        ["i.w3-serif.w3-large",
                            "Perfection is achieved, not when there is nothing more to add, but when there is nothing left to take away."
                        ]
                    ]],
                    ["div.w3-right",
                        [["i.w3-serif", "Antoine de Saint Exupery"]]
                    ]
                ]]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
    }
};
