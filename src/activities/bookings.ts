import { HElement } from "peryl/dist/hsml";
import { HAppActions, HAppI, HFormData } from "peryl/dist/hsml-app";
import { Booking, bookingsAll, bookingsCreate, bookingsDelete, bookingsById, bookingsUpdate } from "../services/bookings";
import { booking } from "./booking";

export interface BookingsState {
    title: string;
    bookings: Booking[];
    form?: HFormData<Booking>;
}

export enum BookingsActions {
    select = "bookings-select",
    edit = "bookings-edit",
    delete = "bookings-delete",
    submit = "bookings-submit",
    cancel = "bookings-cancel"
}

export const bookings: HAppI<BookingsState, BookingsActions> = {

    name: "bookings",

    state: function () {
        return {
            title: "Bookings",
            bookings: []
        };
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", [
                    state.title,
                    !state.form
                        ? ["a.w3-btn.w3-color.w3-right.w3-large",
                            {
                                href: "javascript:void(0)",
                                accesskey: "c",
                                title: "alt-c",
                                on: ["click", BookingsActions.edit]
                            },
                            [["i.fa.fa-plus"]]
                        ]
                        : undefined
                ]],
                !state.form
                    ? ["ul.w3-ul.w3-card-4.w3-white",
                        state.bookings.map<HElement<BookingsActions>>(booking =>
                            ["li.w3-bar", [
                                // ["img.w3-bar-item.w3-circle.w3-hide-small",
                                //     { src: "https://www.w3schools.com/w3css/img_workshop.jpg", style: "width:85px" }
                                // ],
                                ["div.w3-bar-item",
                                    { on: ["click", BookingsActions.select, booking.id] },
                                    [
                                        ["span.w3-large", booking.title],
                                        ["br"],
                                        ["span", booking.location]
                                    ]
                                ],
                                ["div.w3-bar-item.w3-right", [
                                    ["a.w3-button.w3-text-red.w3-right",
                                        {
                                            href: "javascript:void(0)",
                                            on: ["click", BookingsActions.delete, booking.id]
                                        },
                                        [["i.fa.fa-trash"]]
                                    ],
                                    ["a.w3-button.w3-text-color.w3-right",
                                        {
                                            href: "javascript:void(0)",
                                            on: ["click", BookingsActions.edit, booking.id]
                                        },
                                        [["i.fa.fa-edit"]]
                                    ],
                                    ["a.w3-button.w3-text-green.w3-right",
                                        {
                                            href: "javascript:void(0)",
                                            on: ["click", BookingsActions.select, booking.id]
                                        },
                                        [["i.fa.fa-calendar-check-o"]]
                                    ],
                                ]]
                            ]]
                        )
                    ]
                    : ["form.w3-container",
                        {
                            novalidate: true,
                            on: ["submit", BookingsActions.submit]
                        }, [
                        ["p.w3-small", [
                            ["label", ["ID",
                                ["span.w3-input", state.form.data.id]
                            ]],
                            ["input", { type: "hidden", name: "id", value: state.form.data.id }]
                        ]],
                        ["p", [
                            ["label", ["Name",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "title",
                                    required: true,
                                    minlength: 3,
                                    maxlength: 30,
                                    value: state.form.data.title
                                }]
                            ]],
                            ["div.w3-text-red", state.form.validation.title]
                        ]],
                        ["p", [
                            ["label", ["Location",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "location",
                                    required: true,
                                    minlength: 3,
                                    maxlength: 50,
                                    value: state.form.data.location
                                }]
                            ]],
                            ["div.w3-text-red", state.form.validation.location]
                        ]],
                        ["p", [
                            ["button.w3-btn.w3-color",
                                {
                                    accesskey: "a",
                                    title: "alt-a"
                                },
                                "Save"
                            ],
                            " ",
                            ["a.w3-btn.w3-gray.w3-text-white.w3-margin-left",
                                {
                                    href: "javascript:void(0)",
                                    accesskey: "c",
                                    title: "alt-c",
                                    on: ["click", BookingsActions.cancel]
                                },
                                "Cancel"
                            ]
                        ]]
                    ]]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {

            case HAppActions.mount:
                state.bookings = bookingsAll();
                break;

            case BookingsActions.select: {
                const bookingId = action.data as string;
                location.hash = `${booking.name}/${bookingId}`;
                break;
            }

            case BookingsActions.edit: {
                const bookingId = action.data as string;
                if (bookingId) {
                    state.form = {
                        data: bookingsById(bookingId) ?? {
                            title: "",
                            location: ""
                        },
                        validation: {}
                    };
                } else {
                    state.form = {
                        data: {
                            title: "",
                            location: ""
                        },
                        validation: {}
                    };
                }
                break;
            }

            case BookingsActions.submit: {
                const form = action.data as HFormData<Booking>;
                state.form = form;
                if (form.valid) {
                    if (form.data.id) {
                        bookingsUpdate(form.data);
                    } else {
                        bookingsCreate(form.data);
                    }
                    state.form = undefined;
                    state.bookings = bookingsAll();
                }
                break;
            }

            case BookingsActions.cancel:
                state.form = undefined;
                break;

            case BookingsActions.delete: {
                const bookingId = action.data as string;
                bookingsDelete(bookingId);
                state.bookings = bookingsAll();
                break;
            }
        }
    }
};
