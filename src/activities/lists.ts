import { HAppI } from "peryl/dist/hsml-app";

export interface ListsState {
    title: string;
    list: string[];
}

export enum ListsActions {
    select = "lists-select"
}

export const lists: HAppI<ListsState, ListsActions> = {

    name: "lists",

    state: function () {
        return {
            title: "Lists",
            list: [
                ""
            ]
        };
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["div.w3-content.w3-container", [
                    ["ol.list-rounded", [
                        ["li", [
                            ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List item"]
                        ]],
                        ["li", [
                            ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List item"],
                            ["ol", [
                                ["li", [
                                    ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List sub item"]
                                ]],
                                ["li", [
                                    ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List sub item"],
                                    ["ol", [
                                        ["li", [
                                            ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List sub item"]
                                        ]],
                                        ["li", [
                                            ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List sub item"]
                                        ]]
                                    ]]
                                ]],
                                ["li", [
                                    ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List sub item"]
                                ]]
                            ]]
                        ]],
                        ["li", [
                            ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List item"]
                        ]]
                    ]],
                    ["ol.list-rectangle", [
                        ["li", [
                            ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List item"]
                        ]],
                        ["li", [
                            ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List item"],
                            ["ol", [
                                ["li", [
                                    ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List sub item"]
                                ]],
                                ["li", [
                                    ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List sub item"],
                                    ["ol", [
                                        ["li", [
                                            ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List sub item"]
                                        ]],
                                        ["li", [
                                            ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List sub item"]
                                        ]]
                                    ]]
                                ]],
                                ["li", [
                                    ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List sub item"]
                                ]]
                            ]]
                        ]],
                        ["li", [
                            ["a", { href: "javascript:void(0)", on: ["click", ListsActions.select] }, "List item"]
                        ]]
                    ]]
                ]]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {
            case ListsActions.select:
                console.log("selected", action.event);
                break;
        }
    }
};
