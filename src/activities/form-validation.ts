import { HElement, hjoin } from "peryl/dist/hsml";
import { HAppActions, HAppI, HFormData, HFormInputData } from "peryl/dist/hsml-app";
import { BooleanValidator, DateValidator, FormValidator, FormValidatorData, NumberValidator, SelectValidator, StringValidator } from "peryl/dist/validators";
import { HashUrlDataState } from "..";
import { Data } from "../services/data";

export interface FormValidationData {
    name: string;
    born: Date;
    children: number;
    married: boolean;
    gender: string;
    sport: string;
}

export interface FormValidationState extends HashUrlDataState {
    title: string;
    data: FormValidationData;
    genders: {
        label: string;
        value: string;
    }[];
    sports: string[];
    validatorData?: FormValidatorData<FormValidationData>;
}

export enum FormValidationActions {
    change = "form-validation-change",
    submit = "form-validation-submit"
}

export const formValidation: HAppI<FormValidationState, FormValidationActions> = {

    name: "form-validation",

    state: function () {
        return {
            title: "Form Validation",
            data: {
                name: "Ema",
                born: new Date(),
                children: 0,
                married: false,
                gender: "female",
                sport: "gymnastics"
            },
            genders: [
                { label: "Male", value: "male" },
                { label: "Female", value: "female" }
            ],
            sports: ["football", "gymnastics"]
        }
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["form.w3-container",
                    {
                        on: [
                            ["change", FormValidationActions.change],
                            ["submit", FormValidationActions.submit]
                        ]
                    },
                    [
                        ["p", [
                            ["label", ["Name",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "name",
                                    value: state.validatorData!.str.name,
                                    on: ["input", FormValidationActions.change]
                                }]
                            ]],
                            ["div.w3-text-red", [state.validatorData!.err.name]],
                        ]],
                        ["p", [
                            ["label", ["Born",
                                ["input.w3-input", {
                                    type: "datetime-local",
                                    name: "born",
                                    // placeholder: new DateValidator().format(new Date()).str,
                                    value: datetimeLocal(state.validatorData!.obj.born)
                                }]
                            ]],
                            ["div.w3-text-red", [state.validatorData!.err.born]],
                        ]],
                        ["p", [
                            ["label", ["Children",
                                ["input.w3-input", {
                                    type: "number",
                                    name: "children",
                                    step:"any",
                                    value: state.validatorData!.str.children
                                }]
                            ]],
                            ["div.w3-text-red", [state.validatorData!.err.children]],
                        ]],
                        ["p", [
                            ["label", [
                                ["input.w3-check", {
                                    type: "checkbox",
                                    name: "married",
                                    checked: state.validatorData!.obj.married
                                }],
                                " Married"
                            ]],
                            ["div.w3-text-red", [state.validatorData!.err.married]],
                        ]],
                        ["p",
                            hjoin(
                                state.genders.map<HElement<FormValidationActions>>(gender => (
                                    ["label", [
                                        ["input.w3-radio", {
                                            type: "radio",
                                            name: "gender",
                                            value: gender.value,
                                            checked: state.validatorData!.obj.gender === gender.value
                                        }],
                                        " ", gender.label
                                    ]]
                                )),
                                ["br"]
                            )
                        ],
                        ["p.w3-text-red", [state.validatorData!.err.gender]],
                        ["p", [
                            ["select.w3-select", { name: "sport" },
                                [
                                    ["option",
                                        { value: "", disabled: true, selected: true },
                                        "Sport"
                                    ],
                                    ...state.sports.map<HElement<FormValidationActions>>(sport => (
                                        ["option",
                                            {
                                                value: sport,
                                                selected: sport === state.validatorData!.obj.sport
                                            },
                                            sport
                                        ])
                                    )
                                ]
                            ],
                            ["div.w3-text-red", state.validatorData!.err.sport],
                        ]],
                        ["p", [
                            ["button.w3-btn.w3-color", "Submit"]
                        ]]
                    ]
                ]
            ]]
        ];
    },

    dispatcher: async function (action, state) {
        switch (action.type) {

            case HAppActions.init:
                formValidator = validator(state);
                state.validatorData = formValidator.data();
                break;

            case FormValidationActions.change:
                const input = action.data as HFormInputData;
                formValidator.validate({
                    ...state.validatorData!.str,
                    [input.name!]: input.value
                });
                state.validatorData = formValidator.data();
                console.log("obj:", JSON.stringify(state.validatorData, null, 4));
                break;

            case FormValidationActions.submit:
                const form = action.data as HFormData<Data>;
                console.log("form", JSON.stringify(form, null, 4));
                formValidator.validate(form.data as any);
                state.validatorData = formValidator.data();
                console.log("validator data", JSON.stringify(state.validatorData, null, 4));
                state.validatorData = formValidator.data();
                if (formValidator.valid) {
                    console.log(formValidator);
                    state.data = formValidator.obj!;
                    const formData = JSON.stringify(state.data, null, 4);
                    console.dir(formData);
                    alert(`Form submit: \n${formData}`);
                }
                break;
        }
    }
};

let formValidator: FormValidator<FormValidationData>;

export function validator(formState: FormValidationState): FormValidator<FormValidationData> {
    return new FormValidator<FormValidationData>()
        .addValidator("name", new StringValidator(
            {
                required: true,
                min: 3,
                max: 5
            },
            {
                required: "Required {{min}} - {{max}} {{regexp}}",
                invalid_format: "Invalid format {{regexp}}",
                not_in_range: "Not in range {{min}} - {{max}}"
            }))
        .addValidator("born", new DateValidator(
            {
                required: true,
                // min: new Date(),
                max: new Date(),
                dateOnly: false,
                iso: true
            },
            {
                required: "Required {{min}} - {{max}}",
                invalid_format: "Invalid format {{date}}",
                not_in_range: "Not in range {{min}} - {{max}}"
            }))
        .addValidator("children", new NumberValidator(
            {
                required: true,
                min: 0,
                max: 10
            },
            {
                required: "Required {{min}} - {{max}}",
                invalid_format: "Invalid format, use format {{num}}",
                not_in_range: "Not in range {{min}} - {{max}}"
            }))
        .addValidator("gender", new SelectValidator(
            {
                required: true,
                options: formState.genders.map(g => g.value)
            },
            {
                required: "Required {{options}}",
                invalid_option: "Invalod option {{option}} {{options}}"
            }))
        .addValidator("married", new BooleanValidator(
            {
                required: true
            },
            {
                required: "Required",
                invalid_value: "Invalid value {{value}}"
            }))
        .addValidator("sport", new SelectValidator(
            {
                required: true,
                options: formState.sports
            },
            {
                required: "Required {{options}}",
                invalid_option: "Invalod option {{option}} {{options}}"
            }))
        .format(formState.data);
}

// function datetimeLocal(date: Date) {
//     date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
//     return date.toISOString().slice(0, 16);
// }

function datetimeLocal(date: Date) {
    if (!date) {
        return "";
    }
    const tzo = - date.getTimezoneOffset();
    if (tzo === 0) {
        return date.toISOString();
    } else {
        // let dif = tzo >= 0 ? "+" : "-";
        const pad = function (num: number, digits = 2) {
            return String(num).padStart(digits, "0");
        };
        return (
            date.getFullYear() +
            "-" +
            pad(date.getMonth() + 1) +
            "-" +
            pad(date.getDate()) +
            "T" +
            pad(date.getHours()) +
            ":" +
            pad(date.getMinutes())
            // pad(tzo >= 0 ? date.getHours() + (tzo / 60) : date.getHours() - (tzo / 60)) +
            // ":" +
            // pad(tzo >= 0 ? date.getMinutes() + (tzo % 60) : date.getMinutes() - (tzo % 60))
        );
        // return (
        //     date.getFullYear() +
        //     "-" +
        //     pad(date.getMonth() + 1) +
        //     "-" +
        //     pad(date.getDate()) +
        //     "T" +
        //     pad(date.getHours()) +
        //     ":" +
        //     pad(date.getMinutes()) +
        //     ":" +
        //     pad(date.getSeconds()) +
        //     dif +
        //     pad(tzo / 60) +
        //     ":" +
        //     pad(tzo % 60) +
        //     "." +
        //     pad(date.getMilliseconds(), 3)
        // );
    }
}
