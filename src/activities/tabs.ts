import { HAppI } from "peryl/dist/hsml-app";

export interface TabsState {
    title: string;
    tab: "london" | "paris" | "tokyo";
}

export enum TabsAction {
    switch = "tabs-switch"
}

export const tabs: HAppI<TabsState, TabsAction> = {

    name: "tabs",

    state: function () {
        return {
            title: "Tabs",
            tab: "london"
        };
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["div.w3-bar.w3-card", [
                    ["button.w3-bar-item.w3-button.tablink",
                        {
                            classes: [["w3-color", state.tab === "london"]],
                            on: ["click", TabsAction.switch, "london"]
                        },
                        "London"
                    ],
                    ["button.w3-bar-item.w3-button.tablink",
                        {
                            classes: [["w3-color", state.tab === "paris"]],
                            on: ["click", TabsAction.switch, "paris"]
                        },
                        "Paris"
                    ],
                    ["button.w3-bar-item.w3-button.tablink",
                        {
                            classes: [["w3-color", state.tab === "tokyo"]],
                            on: ["click", TabsAction.switch, "tokyo"]
                        },
                        "Tokyo"
                    ]
                ]],
                ["div.w3-container.w3-card.w3-white",
                    { class: state.tab === "london" ? "w3-show" : "w3-hide" },
                    [
                        ["h2", "London"],
                        ["p", "London is the capital city of England."]
                    ]
                ],
                ["div.w3-container.w3-card.w3-white",
                    { class: state.tab === "paris" ? "w3-show" : "w3-hide" },
                    [
                        ["h2", "Paris"],
                        ["p", "Paris is the capital of France."]
                    ]
                ],
                ["div.w3-container.w3-card.w3-white",
                    { class: state.tab === "tokyo" ? "w3-show" : "w3-hide" },
                    [
                        ["h2", "Tokyo"],
                        ["p", "Tokyo is the capital of Japan."]
                    ]
                ]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {
            case TabsAction.switch:
                state.tab = action.data;
                break;
        }
    }
};
