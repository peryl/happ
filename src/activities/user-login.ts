import { HAppActions, HAppI, HFormData } from "peryl/dist/hsml-app";
import { HashUrlDataState } from "..";
import { auth } from "../services/auth";
import { usersByEmail } from "../services/users";
import { _ } from "../translate";
import { userLoginByEmail } from "./user-login-by-email";
import { userRegister } from "./user-register";

export interface UserLoginData {
    email: string;
    password: string;
}

export interface UserLoginState extends HashUrlDataState {
    title: string;
    form: HFormData<UserLoginData>;
    loginError?: string;
    logged?: string;
}

export enum UserLoginActions {
    change = "user-login-change",
    login = "user-login-login",
    logout = "user-login-logout"
}

export const userLogin: HAppI<UserLoginState, UserLoginActions> = {

    name: "user-login",

    state: function () {
        return {
            title: _("User Login"),
            form: {
                data: {
                    email: "",
                    password: ""
                },
                validation: {}
            },
            loginError: ""
        };
    },

    view: function (state) {
        return [
            ["div.w3-content",[
                ["h1", state.title],
                !state.logged
                    ? ["form.w3-container",
                        {
                            novalidate: true,
                            on: [
                                ["change", UserLoginActions.change],
                                ["submit", UserLoginActions.login]
                            ]
                        },
                        [
                            ["p", [
                                ["label", [
                                    _("Email"),
                                    ["a.w3-text-color.w3-margin-left.w3-right",
                                        { href: `#${userRegister.name}` },
                                        _("Register new user")
                                    ],
                                    ["input.w3-input", {
                                        type: "email",
                                        autocomplete: "email",
                                        // placeholder: "Email",
                                        name: "email",
                                        minlength: 3,
                                        maxlength: 50,
                                        required: true,
                                        value: state.form.data.email
                                    }]
                                ]],
                                ["div.w3-text-red", state.form.validation.email]
                            ]],
                            ["p", [
                                ["label", [
                                    _("Password"),
                                    ["a.w3-text-color.w3-margin-left.w3-right",
                                        { href: `#${userLoginByEmail.name}` },
                                        _("Forgot password?")
                                    ],
                                    ["input.w3-input", {
                                        type: "password",
                                        autocomplete: "current-password",
                                        // placeholder: "Current password",
                                        name: "password",
                                        required: true,
                                        minlength: 8,
                                        maxlength: 20,
                                        // pattern: "",
                                        value: state.form.data.password
                                    }]
                                ]],
                                ["div.w3-text-red", state.form.validation.password]
                            ]],
                            ["p.w3-text-red", [state.loginError]],
                            // ["p", [
                            //     ["a.w3-text-color",
                            //         { href: `#${userLoginByEmail.name}` },
                            //         "Forgot password?"
                            //     ]
                            // ]],
                            ["p", [
                                ["button.w3-btn.w3-color", _("Login")],
                                // ["span.w3-padding.w3-right-", [
                                //     ["a.w3-text-color",
                                //         { href: `#${userRegister.name}` },
                                //         [["b", "Register"]]
                                //     ],
                                //     " user account"
                                // ]]
                                " ",
                                ["a.w3-btn.w3-margin-left",
                                    { href: `#` },
                                    _("Cancel")
                                ]
                            ]]
                        ]
                    ]
                    : ["p", [
                        "Logged: ", ["b", state.logged],
                        ["br"],
                        ["br"],
                        ["button.w3-btn.w3-color",
                            { on: ["click", UserLoginActions.logout] },
                            _("Logout")
                        ],
                        " ",
                        ["a.w3-btn.w3-margin-left",
                            { href: `#` },
                            _("Cancel")
                        ]
                    ]]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {

            case HAppActions.init:
                break;

            case HAppActions.mount:
                state.logged = auth.valid() ? auth.get()?.payload.sub : undefined;
                break;

            case UserLoginActions.change:
                console.log(action.data);
                break;

            case UserLoginActions.login:
                const form = action.data as HFormData<UserLoginData>;
                console.log("form", form);
                state.form = form;
                // alert(JSON.stringify(action.data, null, 4));
                const user = usersByEmail(form.data.email);
                if (user && user.password === form.data.password) {
                    auth.set(JSON.stringify({
                        exp: Math.floor(new Date().getTime() / 1e3) + 10,
                        sub: user.email
                    }));
                    const activity = state.hashUrlData?.path[1];
                    if (activity) {
                        location.hash = `#${activity}`;
                    }
                } else {
                    state.loginError = "Wrong login";
                }
                state.logged = auth.valid() ? auth.get()?.payload.sub : undefined;
                break;

            case UserLoginActions.logout:
                auth.set(undefined);
                state.logged = auth.valid() ? auth.get()?.payload.sub : undefined;
                break;
        }
    }
};
