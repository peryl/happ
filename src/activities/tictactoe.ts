import { HElement } from "peryl/dist/hsml";
import { HAppI } from "peryl/dist/hsml-app";

const NBSP = "\u00A0";
const CIRC = "\u25EF";
const CROS = "\u2A2F";

export interface TicTacToeState {
    title: string;
    board: string[][];
    turn: number;
}

export const enum TicTacToeActions {
    mark = "tictactoe-mark",
    reset = "tictactoe-reset",
    noop = "tictactoe-noop"
}

const boardInit = () => [
    [NBSP, NBSP, NBSP],
    [NBSP, NBSP, NBSP],
    [NBSP, NBSP, NBSP]
];

export const ticTacToe: HAppI<TicTacToeState, TicTacToeActions> = {

    name: "tictactoe",

    state: function () {
        return {
            title: "Tic-Tac-Toe",
            board: boardInit(),
            turn: 0
        };
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["div.w3-center", [
                    ["p.w3-xlarge", [
                        "Player: ", state.turn ? CROS : CIRC,
                    ]],
                    ["div", state.board.map<HElement<TicTacToeActions>>((row, y) =>
                        ["div", row.map<HElement<TicTacToeActions>>((col, x) =>
                            ["button.w3-button.w3-white.w3-border.w3-border-gray",
                                {
                                    styles: {
                                        fontFamily: "monospace",
                                        fontSize: "300%",
                                        display: "inline-block",
                                        width: "2em",
                                        height: "2em"
                                    },
                                    on: (col === NBSP)
                                        ? ["click", TicTacToeActions.mark, { x, y, turn: state.turn }]
                                        : ["click", TicTacToeActions.noop]
                                },
                                col
                            ])
                        ])
                    ],
                    ["p", [
                        ["button.w3-button.w3-color", { on: ["click", TicTacToeActions.reset] }, "Reset"]
                    ]]
                ]]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {

            case TicTacToeActions.mark:
                state.board[action.data.y][action.data.x] = action.data.turn ? CROS : CIRC;
                state.turn = action.data.turn ? 0 : 1;
                break;

            case TicTacToeActions.reset:
                state.board = boardInit();
                break;
        }
    }
};
