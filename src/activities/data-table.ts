import { HElement } from "peryl/dist/hsml";
import { HAppActions, HAppI } from "peryl/dist/hsml-app";
import { HashUrlDataState } from "..";
import { Data, dataAll, dataCreate, dataDelete } from "../services/data";
import { dataForm, datetimeLocal } from "./data-form";

export interface DataTableState extends HashUrlDataState {
    title: string;
    data: Data[];
}

const state: DataTableState = {
    title: "Table",
    data: []
};

export enum DataTableActions {
    delete = "data-table-delete",
    page = "data-table-page"
}

export const dataTable: HAppI<DataTableState, DataTableActions> = {

    name: "data-table",

    state: function () {
        return state;
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["p", [
                    ["a.w3-btn.w3-color.w3-text-white",
                        {
                            href: `#${dataForm.name}`,
                            accesskey: "c",
                            title: "alt-c"},
                        "Create data"
                    ]
                ]],
                ["div.w3-responsive.w3-card", [
                    ["table.w3-table-all.w3-hoverable", [
                        ["thead", [
                            ["tr", [
                                ["th", ""],
                                ["th", "ID"],
                                ["th", "Name"],
                                ["th", "Born"],
                                ["th", "Children"],
                                ["th", "Married"],
                                ["th", "Gender"],
                                ["th", "Sport"],
                                ["th", ""]
                            ]]
                        ]],
                        ["tbody",
                            state.data.map<HElement<DataTableActions>>(d => ["tr", [
                                ["td", [
                                    ["a.w3-text-color",
                                        {
                                            href: `#data-form/${encodeURIComponent(d.id!)}`,
                                            title: "Edit"
                                        },
                                        [["i.fa.fa-edit"]]
                                    ]
                                ]],
                                ["td", d.id],
                                ["td", d.name],
                                ["td", new Date(d.born).toLocaleDateString(undefined, { dateStyle: "full" })],
                                ["td", d.children],
                                ["td", d.married],
                                ["td", d.gender],
                                ["td", d.sport],
                                ["td", [
                                    ["a.w3-text-red",
                                        {
                                            href: "javascript:void(0)",
                                            on: ["click", DataTableActions.delete, d.id],
                                            title: "Delete"
                                        },
                                        [["i.fa.fa-trash"]]
                                    ]
                                ]]
                            ]])
                        ]
                    ]]
                ]],
                ["p.w3-bar", [
                    ["a.w3-button", { href: "javascript:void(0)", on: ["click", DataTableActions.page, "<"] }, "«" ],
                    ["a.w3-button", { href: "javascript:void(0)", on: ["click", DataTableActions.page, "1"] }, "1" ],
                    ["a.w3-button", { href: "javascript:void(0)", on: ["click", DataTableActions.page, "2"], class: "w3-color" }, "2" ],
                    ["a.w3-button", { href: "javascript:void(0)", on: ["click", DataTableActions.page, "3"] }, "3" ],
                    ["a.w3-button", { href: "javascript:void(0)", on: ["click", DataTableActions.page, "4"] }, "4" ],
                    ["a.w3-button", { href: "javascript:void(0)", on: ["click", DataTableActions.page, ">"] }, "»" ]
                ]]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {
            case HAppActions.init:
                if (!dataAll().length) {
                    dataCreate({
                        name: "Ema",
                        born: datetimeLocal(new Date()),
                        children: 0,
                        married: false,
                        gender: "female",
                        sport: "gymnastics"
                    });
                    dataCreate({
                        name: "Arthur",
                        born: datetimeLocal(new Date()),
                        children: 0,
                        married: false,
                        gender: "male",
                        sport: "football"
                    });
                    dataCreate({
                        name: "Maria",
                        born: datetimeLocal(new Date()),
                        children: 2,
                        married: true,
                        gender: "female",
                        sport: "gymnastics"
                    });
                }
                state.data = dataAll();
                break;

            case DataTableActions.delete:
                dataDelete(action.data);
                state.data = dataAll();
                break;

            case DataTableActions.page:
                console.log("page", action.data);
                break;
            }
    }
};
