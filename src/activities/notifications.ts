import { HAppActions, HAppI, HDispatchScopes, HFormData } from "peryl/dist/hsml-app";
import { FCM } from "../services/fcm";
import { PushyMe } from "../services/pushyme";

export interface NotificationsState {
    title: string;
    pushyDeviceToken: string;
    pushyApiKey: string;
    fcmCurrentToken: string;
    fcmServerKey: string;
}

export enum NotificationsActions {
    snackbar = "notifications-snackbar",
    browser = "notifications-browser",
    pushy = "notifications-pushy",
    fcm = "notifications-fcm",
    message = "notifications-message"
}

export const notifications: HAppI<NotificationsState, NotificationsActions> = {

    name: "notifications",

    state: function () {
        return {
            title: "Notifications",
            pushyDeviceToken: "",
            pushyApiKey: "",
            fcmCurrentToken: "",
            fcmServerKey: ""
        };
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["h2", "Snackbar notification"],
                ["form.w3-container", { on: ["submit", NotificationsActions.snackbar] },
                    [
                        ["p", [
                            ["label", [
                                "Message",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "message",
                                    value: "Snackbar message"
                                }]
                            ]]
                        ]],
                        ["button.w3-btn.w3-color", "Send"]
                    ]
                ],
                ["h2", "Browser notification"],
                ["form.w3-container", { on: ["submit", NotificationsActions.browser] },
                    [
                        ["p", [
                            ["label", [
                                "Title",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "title",
                                    value: "Browser title"
                                }]
                            ]]
                        ]],
                        ["p", [
                            ["label", [
                                "Message",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "message",
                                    value: "Browser message"
                                }]
                            ]]
                        ]],
                        ["button.w3-btn.w3-color", "Send"]
                    ]
                ],
                ["h2", "Pushy notification"],
                ["form.w3-container", { on: ["submit", NotificationsActions.pushy] },
                    [
                        ["p", [
                            ["label", [
                                "Device token",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "device",
                                    value: state.pushyDeviceToken,
                                    disabled: true
                                }]
                            ]],
                        ]],
                        ["p", [
                            ["label", [
                                "API key",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "apiKey",
                                    value: state.pushyApiKey
                                }]
                            ]],
                        ]],
                        ["p", [
                            ["label", [
                                "Title",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "title",
                                    value: "Pushy title"
                                }]
                            ]]
                        ]],
                        ["p", [
                            ["label", [
                                "Message",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "message",
                                    value: "Pushy message"
                                }]
                            ]]
                        ]],
                        ["p", [
                            ["label", [
                                "URL",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "url",
                                    value: "https://gitlab.com/peryl/happ"
                                }]
                            ]]
                        ]],
                        ["p", [
                            ["button.w3-btn.w3-color", "Send"]
                        ]]
                    ]
                ],
                ["h2", "FCM notification"],
                ["form.w3-container", { on: ["submit", NotificationsActions.fcm] },
                    [
                        ["p", [
                            ["label", [
                                "Current token",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "device",
                                    value: state.fcmCurrentToken,
                                    disabled: true
                                }]
                            ]],
                        ]],
                        ["p", [
                            ["label", [
                                "Server key",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "serverKey",
                                    value: state.fcmServerKey
                                }]
                            ]],
                        ]],
                        ["p", [
                            ["label", [
                                "Title",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "title",
                                    value: "FCM Title"
                                }]
                            ]]
                        ]],
                        ["p", [
                            ["label", [
                                "Message",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "message",
                                    value: "FCM Message"
                                }]
                            ]]
                        ]],
                        ["p", [
                            ["label", [
                                "URL",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "url",
                                    value: "https://gitlab.com/peryl/happ"
                                }]
                            ]]
                        ]],
                        ["p", [
                            ["button.w3-btn.w3-color", "Send"]
                        ]]
                    ]
                ]
            ]]
        ]
    },

    dispatcher: async function (this, action, state, dispatch) {
        switch (action.type) {

            case HAppActions.mount: {
                const appId = "5dca8013574cb62d339c084f";
                pushyMe = new PushyMe(
                    appId,
                    (deviceToken) => {
                        state.pushyDeviceToken = deviceToken;
                        this.update();
                    },
                    (data) => {
                        console.log("Pushy data: " + JSON.stringify(data));
                        const message = `${data.title}: ${data.message}`;
                        dispatch(NotificationsActions.message, message, HDispatchScopes.window);
                    });
                (window as any).pushy = pushyMe;
                state.pushyApiKey = localStorage.getItem(pushyApiKey) || "";


                // https://console.firebase.google.com/u/0/project/peryl-pwa/settings/general/web:MTUyODE1ZmEtNmE1Ny00ZWQzLTg5MWItNWQwNzExOTFhNjcw
                const firebaseConfig = {
                    apiKey: "AIzaSyCnrSMsBbiDAdO9FUNWsrDaGpoWSsZL8vA",
                    authDomain: "peryl-pwa.firebaseapp.com",
                    projectId: "peryl-pwa",
                    storageBucket: "peryl-pwa.appspot.com",
                    messagingSenderId: "109207901144",
                    appId: "1:109207901144:web:3d7f6ce04fee96790218ad"
                };
                // https://console.firebase.google.com/u/0/project/peryl-pwa/settings/cloudmessaging
                const vapidKey = "BC6_48OGVJzMK0tXCHX-Rom6NlR3YdMz7IquL7D9iucCc3SSrJQ7oZOOjsMHP58ZSiAceZ4nurvE04r6LKjRyhA";
                fcm = new FCM(firebaseConfig, vapidKey,
                    async (currentToken) => {
                        state.fcmCurrentToken = currentToken;
                        localStorage.setItem(fcmCurrentToken, currentToken);
                        this.update();
                        // await UserService.userFcmTokenSave({ token: currentToken });
                    },
                    async (payload) => {
                        console.log("FCM payload: " + JSON.stringify(payload));
                        const message = `${payload.notification!.title}, ${payload.notification!.body}`;
                        dispatch(NotificationsActions.message, message, HDispatchScopes.window);
                    });
                (window as any).fcm = fcm;
                state.fcmServerKey = localStorage.getItem(fcmServerKey) || "";
                break;
            }

            case NotificationsActions.snackbar: {
                const form = action.data;
                dispatch(NotificationsActions.message, form.data.message, HDispatchScopes.window);
                break;
            }

            case NotificationsActions.browser: {
                const form = action.data as HFormData<{ title: string; }>;
                browserNotification(form.data.title, action.data.message);
                break;
            }

            case NotificationsActions.pushy: {
                const form = action.data as HFormData<any>;
                form.data.apiKey &&
                    localStorage.setItem(pushyApiKey, form.data.apiKey);
                pushyMe.push(
                    form.data.apiKey,
                    form.data.title,
                    form.data.message,
                    form.data.url);
                break;
            }

            case NotificationsActions.fcm: {
                const form = action.data as HFormData<any>;
                form.data.serverKey &&
                    localStorage.setItem(fcmServerKey, form.data.serverKey);
                fcm.push(
                    form.data.serverKey,
                    form.data.title,
                    form.data.message,
                    form.data.url);
                break;
            }
        }
    }
};

const pushyApiKey = "pushyApiKey";

let pushyMe: PushyMe;

const fcmServerKey = "fcmServerKey";
const fcmCurrentToken = "fcmCurrentToken";

let fcm: FCM;

function browserNotification(title: string, message: string) {
    showNotification(title, {
        body: message,
        // icon: "images/icon.png",
        // badge: "images/badge.png",
        vibrate: [100, 50, 100],
        data: {
            onAction: {
                // "": location.href,
                like: "http://bbb.sk",
                reply: "http://ccc.sk"
            }
        },
        actions: [
            { action: "like", title: "Like" },
            { action: "reply", title: "Reply" }
        ]
    });
}

function showNotification(title: string, options?: NotificationOptions): void {
    if ((self as any).Notification) {
        // console.log("Notification supported");
        Notification.requestPermission(permission => {
            if (permission === "granted") {
                // console.log("Notification permission granted");
                navigator.serviceWorker.ready.then(registration => {
                    // console.log("Notification SW ready");
                    registration.showNotification(title, options);
                    // registration.addEventListener("updatefound", e => {
                    //     console.log("Registration updatefound", e);
                    //     registration.update();
                    // });
                });
            } else {
                console.warn("Notification permission:", permission);
            }
        });
    } else {
        console.warn("Notification not supported");
    }
}
