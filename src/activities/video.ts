import { HAppActions, HAppI } from "peryl/dist/hsml-app";
import { HashUrlDataState } from "..";

export interface VideoState extends HashUrlDataState {
    title: string;
    qrcode?: string;
}

export enum VideoActions {
    takePhoto = "qrscan-take-photo"
}

export const video: HAppI<VideoState, VideoActions> = {

    name: "video",

    state: function () {
        return {
            title: "Video"
        }
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["h2", "Video"],
                ["p", [
                    ["video~video",
                        { style: "width:100%", controls: false, autoplay: true, playsinline: true },
                        "You need a camera in order to use this app."
                    ]
                ]],
                ["p", [
                    ["button.w3-button.w3-color",
                        { style: "width:100%", on: ["click", VideoActions.takePhoto] },
                        "Take a picture"
                    ]
                ]],
                ["h2", "Canvas"],
                ["p", [
                    ["canvas~canvas", { style: "width:100%" }, " "]
                ]],
                ["h2", "Image"],
                ["p", [
                    ["img~image", { style: "width:100%", src: "//:0", alt: "" }],
                ]],
                ["p", [
                    // ["input", { type: "file", name: "image", accept: "image/*", capture: "user" }],
                    // ["input", { type: "file", name: "video", accept: "video/*", capture: "environment" }]
                ]]
            ]]
        ];
    },

    dispatcher: async function (this, action, state, dispatch) {
        switch (action.type) {

            case HAppActions.mount:
                // start camera
                const constraints = { video: { facingMode: "environment" }, audio: false };
                navigator.mediaDevices
                    .getUserMedia(constraints)
                    .then(stream => this.refs["video"].srcObject = stream)
                    .catch(error => console.error(error));
                break;

            case HAppActions.umount:
                this.refs["video"].srcObject
                    .getTracks()
                    .forEach(track => track.stop());
                this.refs["video"].srcObject = null;
                break

            case VideoActions.takePhoto:
                const video = this.refs["video"] as HTMLVideoElement;
                const canvas = this.refs["canvas"] as HTMLCanvasElement;
                const image = this.refs["image"] as HTMLImageElement;

                canvas.width = video.videoWidth;
                canvas.height = video.videoHeight;
                canvas.getContext("2d")!.drawImage(video, 0, 0);

                image.src = canvas.toDataURL("image/png");
                break;
        }
    }
};
