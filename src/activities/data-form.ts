import { HElement, hjoin } from "peryl/dist/hsml";
import { HAppActions, HAppI, HFormData, HFormInputData } from "peryl/dist/hsml-app";
import { HashUrlDataState } from "..";
import { Data, dataCreate, dataSelect, dataUpdate } from "../services/data";
import { dataTable } from "./data-table";

export interface DataFormState extends HashUrlDataState {
    title: string;
    form: HFormData<Data>;
    genders: {
        label: string;
        value: string;
    }[];
    sports: string[];
}

export enum DataFormActions {
    cahnge = "data-form-cahnge",
    submit = "data-form-submit"
}

export const dataForm: HAppI<DataFormState, DataFormActions> = {

    name: "data-form",

    state: function () {
        return {
            title: "Form",
            form: {
                data: {
                    name: "",
                    born: "",
                    children: 0,
                    married: false,
                    gender: "",
                    sport: ""
                },
                validation:{}
            },
            genders: [
                { label: "Male", value: "male" },
                { label: "Female", value: "female" }
            ],
            sports: ["football", "gymnastics"]
        }
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["form.w3-container",
                    {
                        novalidate: true,
                        on: [
                            // ["change", DataFormActions.cahnge],
                            ["input", DataFormActions.cahnge],
                            ["submit", DataFormActions.submit]
                        ]
                    }, [
                    ["p.w3-small", [
                        ["label", ["ID",
                            ["span.w3-input", state.form.data.id]
                        ]],
                        ["input", { type: "hidden", name: "id", value: state.form.data.id }]
                    ]],
                    ["p", [
                        ["label", ["Name",
                            ["input.w3-input", {
                                type: "text",
                                name: "name",
                                required: true,
                                minlength: 3,
                                maxlength: 25,
                                pattern: "[A-Za-z]+\ ?[A-Za-z]*",
                                value: state.form.data.name
                            }]
                        ]],
                        ["div.w3-text-gray", "First name and optional last name, space separated aphabetic only"],
                        ["div.w3-text-red", state.form.validation.name]
                    ]],
                    ["p", [
                        ["label", ["Born",
                            ["input.w3-input", {
                                type: "datetime-local",
                                name: "born",
                                required: true,
                                max: datetimeLocal(new Date()),
                                value: state.form.data.born,
                            }]
                        ]],
                        ["div.w3-text-red", state.form.validation.born]
                    ]],
                    ["p", [
                        ["label", ["Children",
                            ["input.w3-input", {
                                type: "number",
                                name: "children",
                                min: 0,
                                max: 10,
                                value: state.form.data.children
                            }]
                        ]],
                        ["div.w3-text-red", state.form.validation.children]
                    ]],
                    ["p", [
                        ["label", [
                            ["input.w3-check", {
                                type: "checkbox",
                                name: "married",
                                checked: state.form.data.married || false
                            }],
                            " Married"
                        ]],
                        ["div.w3-text-red", state.form.validation.married]
                    ]],
                    ["p", [
                        ["label", "Gender"],
                        ["br"],
                        ...hjoin(
                            state.genders.map<HElement<DataFormActions>>(gender => (
                                ["label", [
                                    ["input.w3-radio", {
                                        type: "radio",
                                        name: "gender",
                                        value: gender.value,
                                        required: true,
                                        checked: state.form.data.gender === gender.value
                                    }],
                                    " ", gender.label
                                ]]
                            )),
                            ["br"]
                        ),
                        ["div.w3-text-red", state.form.validation.gender]
                    ]],
                    ["p", [
                        ["label", [
                            "Sport",
                            ["select.w3-select", {
                                    name: "sport",
                                    required: false
                                },
                                [
                                    ["option",
                                        {
                                            value: "",
                                            selected: true
                                        }
                                    ],
                                    ...state.sports.map<HElement<DataFormActions>>(sport => (
                                        ["option",
                                            {
                                                value: sport,
                                                selected: sport === state.form.data.sport
                                            },
                                            sport
                                        ])
                                    )
                                ]
                            ]
                        ]],
                        ["div.w3-text-red", state.form.validation.sport]
                    ]],
                    ["p", [
                        ["button.w3-btn.w3-color",
                            {
                                accesskey: "a",
                                title: "alt-a"
                            },
                            "Save"
                        ],
                        " ",
                        ["a.w3-btn.w3-gray.w3-text-white.w3-margin-left",
                            {
                                href: `#${dataTable.name}`,
                                accesskey: "c",
                                title: "alt-c"
                            },
                            "Cancel"
                        ]
                    ]]
                ]]
            ]]
        ];
    },

    dispatcher: async function (action, state) {
        switch (action.type) {

            case HAppActions.mount:
                const id = state.hashUrlData?.path[1];
                if (id) {
                    const d = dataSelect(id);
                    if (d) {
                        state.form.data = d;
                    }
                }
                break;

            case DataFormActions.cahnge:
                const input = action.data as HFormInputData;
                console.log("change", JSON.stringify(input, null, 4));
                state.form.data[input.name!] = input.value;
                state.form.validation[input.name!] = input.validation;
                break;

            case DataFormActions.submit:
                const form = action.data as HFormData<Data>;
                console.log("form", JSON.stringify(form, null, 4));
                state.form = form;
                if (form.valid) {
                    if (form.data.id) {
                        dataUpdate(form.data);
                    } else {
                        dataCreate(form.data);
                    }
                    location.hash = `#${dataTable.name}`;
                }
            break;
        }
    }
};


export function datetimeLocal(date: Date): string {
    date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
    return date.toISOString().slice(0, 16);
}
