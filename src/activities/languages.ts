import { HElement, hjoin } from "peryl/dist/hsml";
import { HAppI } from "peryl/dist/hsml-app";
import { HashUrlDataState } from "..";
import { _ } from "../translate";

export interface LanguagesState extends HashUrlDataState {
    title: string;
    langs: {
        locale: string;
        label: string;
    }[]
}

export const languages: HAppI<LanguagesState, string> = {

    name: "languages",

    state: function () {
        return {
            title: _("Languages"),
            langs: [
                { locale: "default", label: _("Browser") },
                { locale: navigator.language, label: "navigator.languages " + JSON.stringify(navigator.languages) },
                { locale: "ar-SA", label: "Arabic (Saudi Arabia)" },
                { locale: "bn-BD", label: "Bangla (Bangladesh)" },
                { locale: "bn-IN", label: "Bangla (India)" },
                { locale: "cs-CZ", label: "Czech (Czech Republic)" },
                { locale: "da-DK", label: "Danish (Denmark)" },
                { locale: "de-AT", label: "Austrian German" },
                { locale: "de-CH", label: "'Swiss' German" },
                { locale: "de-DE", label: "Standard German (as spoken in Germany)" },
                { locale: "el-GR", label: "Modern Greek" },
                { locale: "en-AU", label: "Australian English" },
                { locale: "en-CA", label: "Canadian English" },
                { locale: "en-GB", label: "British English" },
                { locale: "en-IE", label: "Irish English" },
                { locale: "en-IN", label: "Indian English" },
                { locale: "en-NZ", label: "New Zealand English" },
                { locale: "en-US", label: "US English" },
                { locale: "en-ZA", label: "English (South Africa)" },
                { locale: "es-AR", label: "Argentine Spanish" },
                { locale: "es-CL", label: "Chilean Spanish" },
                { locale: "es-CO", label: "Colombian Spanish" },
                { locale: "es-ES", label: "Castilian Spanish (as spoken in Central-Northern Spain)" },
                { locale: "es-MX", label: "Mexican Spanish" },
                { locale: "es-US", label: "American Spanish" },
                { locale: "fi-FI", label: "Finnish (Finland)" },
                { locale: "fr-BE", label: "Belgian French" },
                { locale: "fr-CA", label: "Canadian French" },
                { locale: "fr-CH", label: "'Swiss' French" },
                { locale: "fr-FR", label: "Standard French (especially in France)" },
                { locale: "he-IL", label: "Hebrew (Israel)" },
                { locale: "hi-IN", label: "Hindi (India)" },
                { locale: "hu-HU", label: "Hungarian (Hungary)" },
                { locale: "id-ID", label: "Indonesian (Indonesia)" },
                { locale: "it-CH", label: "'Swiss' Italian" },
                { locale: "it-IT", label: "Standard Italian (as spoken in Italy)" },
                { locale: "ja-JP", label: "Japanese (Japan)" },
                { locale: "ko-KR", label: "Korean (Republic of Korea)" },
                { locale: "nl-BE", label: "Belgian Dutch" },
                { locale: "nl-NL", label: "Standard Dutch (as spoken in The Netherlands)" },
                { locale: "no-NO", label: "Norwegian (Norway)" },
                { locale: "pl-PL", label: "Polish (Poland)" },
                { locale: "pt-BR", label: "Brazilian Portuguese" },
                { locale: "pt-PT", label: "European Portuguese (as written and spoken in Portugal)" },
                { locale: "ro-RO", label: "Romanian (Romania)" },
                { locale: "ru-RU", label: "Russian (Russian Federation)" },
                { locale: "sk-SK", label: "Slovak (Slovakia)" },
                { locale: "sv-SE", label: "Swedish (Sweden)" },
                { locale: "ta-IN", label: "Indian Tamil" },
                { locale: "ta-LK", label: "Sri Lankan Tamil" },
                { locale: "th-TH", label: "Thai (Thailand)" },
                { locale: "tr-TR", label: "Turkish (Turkey)" },
                { locale: "zh-CN", label: "Mainland China, simplified characters" },
                { locale: "zh-HK", label: "Hong Kong, traditional characters" },
                { locale: "zh-TW", label: "Taiwan, traditional characters" }
            ]
        }
    },

    view: function (state) {
        const locale = new Intl.Locale(navigator.language);
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["p", [
                    "navigator.language: ", navigator.language,
                    ["br"],
                    "navigator.languages: ", navigator.languages.toLocaleString(),
                    ["br"],
                    ["br"],
                    "Intl.Locale baseName: ", locale.baseName,
                    ["br"],
                    "Intl.Locale language: ", locale.language,
                    ["br"],
                    "Intl.Locale region: ", locale.region,
                    ["br"],
                    "Intl.Locale region: ", new Intl
                        .DisplayNames(navigator.language, { type: "language" })
                        .of(navigator.language),
                    ["br"],
                    "Intl.Locale timeZones: ", (locale as any)?.timeZones?.toString()
                ]],
                ["p", hjoin(
                    state.langs.map<HElement<any>>(lang =>
                        ["a",
                            {
                                href: `#${languages.name}/${lang.locale}`,
                                title: lang.label
                            },
                            lang.locale]),
                    ", ")
                ],
                ...state.langs.map<HElement<any>>(lang =>
                    ["div.w3-card.w3-padding.w3-white",
                        {
                            id: `${languages.name}/${lang.locale}`,
                            lang: lang.locale
                        },
                        [
                            ["h3", [lang.locale?.toString(), " - ", lang.label]],
                            ["h5", _("Dates")],
                            ["p", [
                                new Date().toLocaleString(lang.locale),
                                ["br"],
                                new Date().toLocaleString(lang.locale, { dateStyle: "full", timeStyle: "full" }),
                                ["br"],
                                new Date().toLocaleString(lang.locale, { dateStyle: "long", timeStyle: "long" }),
                                ["br"],
                                new Date().toLocaleString(lang.locale, { dateStyle: "medium", timeStyle: "medium" }),
                                ["br"],
                                new Date().toLocaleString(lang.locale, { dateStyle: "short", timeStyle: "short" })
                            ]],
                            ["h5", _("Numbers")],
                            ["p", [
                                1234.567.toLocaleString(lang.locale),
                                ["br"],
                                1234.567.toLocaleString(lang.locale, { style: "percent" }),
                                ["br"],
                                1234.567.toLocaleString(lang.locale, { style: "currency", currency: "EUR" }),
                                ["br"],
                                1234.567.toLocaleString(lang.locale, { style: "currency", currency: "USD" })
                            ]
                        ]
                    ]]
                )
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
    }
};
