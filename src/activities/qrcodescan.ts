import { Html5QrcodeScanner } from "html5-qrcode";
import { HAppActions, HAppI } from "peryl/dist/hsml-app";

export interface QrCodeScanState {
    title: string;
    scan: boolean;
    qrcode: string;
    data: string;
}

export enum QrCodeScanActions {
    scan = "qrcodescan-scan",
    scaned = "qrcodescan-scaned"
}

export const qrCodeScan: HAppI<QrCodeScanState, QrCodeScanActions> = {

    name: "qrcodescan",

    state: function () {
        return {
            title: "QR Code Scan",
            scan: false,
            qrcode: "",
            data: ""
        }
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["div#qr-scaner", { skip: true, style: "width:100%" }],
                ["p", [
                    !state.scan
                        ? ["button.w3-button.w3-color",
                            { on: ["click", QrCodeScanActions.scan] },
                            "Scan QR code"
                        ]
                        : undefined
                ]],
                ["p", [
                    "QR Code data: ",
                    ["b", [
                        state.qrcode.startsWith("http")
                            ? ["a",
                                { href: state.qrcode, target: "_blank" },
                                state.qrcode
                            ]
                            : state.qrcode
                    ]]
                ]],
                !!state.data
                    ? ["div", [
                        ["h3", "Pokladničný doklad - data"],
                        ["p", "Pokladničný doklad v systéme e-kasa (podľa zákona č. 289/2008 Z. z. o používaní elektronickej registračnej pokladnice platného od 1.4.2015)"],
                        ["pre", state.data]
                    ]]
                    : undefined
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {

            case HAppActions.umount:
                this.scanner?.clear();
                break;

            case QrCodeScanActions.scan:
                state.qrcode = "";
                state.data = "";
                state.scan = true;
                function onScanSuccess(decodedText, decodedResult) {
                    console.log(`Scan result: ${decodedText}`, decodedResult);
                    dispatch(QrCodeScanActions.scaned, decodedText)
                    // scanner.stop()
                    scanner.clear();
                }
                function onScanError(errorMessage) {
                    // console.error(errorMessage);
                }
                const scanner = new Html5QrcodeScanner(
                    "qr-scaner",
                    {
                        fps: 10,
                        qrbox: (viewfinderWidth, viewfinderHeight) => {
                            return {
                                width: viewfinderWidth * 0.9,
                                height: viewfinderHeight * 0.9
                            };
                        }
                        // formatsToSupport: [Html5QrcodeSupportedFormats.QR_CODE]
                    },
                    false
                );
                scanner.render(onScanSuccess, onScanError);
                // const scanner = new Html5Qrcode(
                //     "qr-scaner",
                //     {
                //         formatsToSupport: [Html5QrcodeSupportedFormats.QR_CODE],
                //         verbose: false
                //     });
                // scanner.start(
                //     { facingMode: "environment"},
                //     {
                //         fps: 10,
                //         qrbox: (viewfinderWidth, viewfinderHeight) => {
                //             return {
                //                 width: viewfinderWidth * 0.9,
                //                 height: viewfinderHeight * 0.9
                //             };
                //         }
                //     },
                //     onScanSuccess,
                //     onScanError);
                this.scanner = scanner;
                break;

            case QrCodeScanActions.scaned:
                state.qrcode = action.data;
                state.scan = false;
                if (/O-.*/.test(state.qrcode)) {
                    const response = await fetch(
                        "https://ekasa.financnasprava.sk/mdu/api/v1/opd/receipt/find",
                        {
                            headers: {
                                accept: "application/json, text/plain, */*",
                                // "accept-language":
                                //     "sk,en-US;q=0.9,en;q=0.8,de;q=0.7",
                                // "cache-control": "no-cache",
                                // "sec-ch-ua-mobile": "?0",
                                "content-type": "application/json;charset=UTF-8",
                                // ignoreloadingbar: "true",
                                // pragma: "no-cache",
                                // "sec-ch-ua":
                                //     '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
                                // "sec-ch-ua-platform": '"Linux"',
                                // "sec-fetch-dest": "empty",
                                // "sec-fetch-mode": "cors",
                                // "sec-fetch-site": "same-site",
                            },
                            // referrer: "https://opd.financnasprava.sk/",
                            // referrerPolicy: "strict-origin-when-cross-origin",
                            body: `{"receiptId":"${state.qrcode}"}`,
                            method: "POST",
                            mode: "cors",
                            credentials: "omit",
                        }
                    );
                    const jsonData = await response.json();
                    state.data = JSON.stringify(jsonData, null, 4);
                }
        }
    }
};
