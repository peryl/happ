import { HAppActions, HAppI, HFormData } from "peryl/dist/hsml-app";
import { userLogin } from "./user-login";
import { _ } from "../translate";

export interface UserLoginByEmailData {
    email: string;
}

export interface UserLoginByEmailState {
    title: string;
    form: HFormData<UserLoginByEmailData>;
    message: string;
    loginEmailError?: string;
}

export enum UserLoginByEmailActions {
    change = "user-login-by-email-change",
    submit = "user-login-by-email-submit"
}

export const userLoginByEmail: HAppI<UserLoginByEmailState, UserLoginByEmailActions> = {

    name: "user-login-by-email",

    state: function () {
        return {
            title: _("User Login by Email"),
            form: {
                data: {
                    email: ""
                },
                validation: {}
            },
            message: ""
        };
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                !state.message
                    ? ["form.w3-container",
                        {
                            novalidate: true,
                            on: [
                                ["change", UserLoginByEmailActions.change],
                                ["submit", UserLoginByEmailActions.submit]
                            ]
                        },
                        [
                            ["p", [
                                ["label", [_("Email"),
                                    ["input.w3-input", {
                                        type: "email",
                                        autocomplete: "email",
                                        // placeholder: "Email",
                                        name: "email",
                                        minlength: 3,
                                        maxlength: 50,
                                        required: true,
                                        value: state.form.data.email
                                    }]
                                ]],
                                ["div.w3-text-red", state.form.validation.email]
                            ]],
                            ["p.w3-text-red", state.loginEmailError],
                            ["p", [
                                ["button.w3-btn.w3-color", _("Login")],
                                " ",
                                ["a.w3-btn.w3-margin-left",
                                    { href: `#${userLogin.name}` },
                                    _("Cancel")
                                ]
                            ]]
                        ]
                    ]
                    : null,
                ["p", state.message]
            ]]
        ];
    },

    dispatcher: async (action, state, dispatch) => {
        switch (action.type) {

            case HAppActions.init:
                break;

            case HAppActions.mount:
                break;

            case UserLoginByEmailActions.change:
                console.log(action.data);
                break;

            case UserLoginByEmailActions.submit:
                const form = action.data as HFormData<UserLoginByEmailData>;
                console.log("form", form);
                state.form = form;
                if (form.valid) {
                    alert(JSON.stringify(action.data, null, 4));
                }
                break;
        }
    }
};
