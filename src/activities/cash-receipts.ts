import { Html5Qrcode, Html5QrcodeScanner, Html5QrcodeSupportedFormats } from "html5-qrcode";
import { Html5QrcodeCameraScanConfig } from "html5-qrcode/esm/html5-qrcode";
import { HElement, NBSP } from "peryl/dist/hsml";
import { HAppActions, HAppI } from "peryl/dist/hsml-app";

// https://eblocky.sk/stanovisko-pravnika/

export interface CashReceiptsState {
    title: string;
    scan: boolean;
    qrcode: string;
    data: string;
    receipts: Reciept[];
}

export enum CashReceiptsActions {
    scan = "cash-receipts-scan",
    scaned = "cash-receipts-scaned",
    view = "cash-receipts-view",
    close = "cash-receipts-close",
    delete = "cash-receipts-delete",
    import = "cash-receipts-import"
}

export const cashReceipts: HAppI<CashReceiptsState, CashReceiptsActions> = {

    name: "cash-receipts",

    state: function () {
        return {
            title: "Cash Receipts",
            scan: false,
            qrcode: "",
            data: "",
            receipts: []
        }
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["div#qr-scaner", { skip: true, style: "width:100%" }],
                !state.data
                    ? ["p", [
                        !state.scan
                            ? ["button.w3-button.w3-color",
                                { on: ["click", CashReceiptsActions.scan] },
                                [["i.fa.fa-qrcode"], NBSP, "Scan cash receipt QR code"]
                            ]
                            : undefined
                    ]]
                    : undefined,
                !!state.qrcode
                    ? ["p", [
                        "QR Code data: ",
                        ["b", [
                            state.qrcode.startsWith("http")
                                ? ["a",
                                    { href: state.qrcode, target: "_blank" },
                                    state.qrcode
                                ]
                                : state.qrcode
                        ]]
                    ]]
                    : undefined,
                !!state.data
                    ? ["div", [
                        ["h3", "Cash Receipt"],
                        ["button.w3-button.w3-color", { on: ["click", CashReceiptsActions.close] }, "Close"],
                        ["pre", state.data]
                    ]]
                    : ["div", [
                        ["div.w3-responsive.w3-card", [
                            ["table.w3-table-all.w3-hoverable", [
                                ["thead", [
                                    ["tr", [
                                        ["th", ""],
                                        // ["th", "ID"],
                                        ["th", "Date"],
                                        ["th", "Price"],
                                        ["th", "Organization"],
                                        ["th", "Unit"],
                                        ["th", ""]
                                    ]]
                                ]],
                                ["tbody",
                                    state.receipts.map<HElement<CashReceiptsActions>>(r => ["tr", [
                                        ["td", [
                                            ["a.w3-text-color",
                                                {
                                                    href: "javascript:void(0)",
                                                    on: ["click", CashReceiptsActions.view, r.receipt.receiptId],
                                                    title: `View ${r.receipt.receiptId}`
                                                },
                                                [["i.fa.fa-eye"]]
                                            ]
                                        ]],
                                        // ["td", r.receipt.receiptId],
                                        ["td", r.receipt.issueDate],
                                        ["td", r.receipt.totalPrice],
                                        ["td", r.receipt.organization.name],
                                        ["td", `${r.receipt.unit.municipality}, ${r.receipt.unit.streetName} ${r.receipt.unit.propertyRegistrationNumber ?? ""}`],
                                        ["td", [
                                            ["a.w3-text-red",
                                                {
                                                    href: "javascript:void(0)",
                                                    on: ["click", CashReceiptsActions.delete, r.receipt.receiptId],
                                                    title: "Delete"
                                                },
                                                [["i.fa.fa-trash"]]
                                            ]
                                        ]]
                                    ]])
                                ]
                            ]]
                        ]],
                        ["p", [
                            ["a.w3-button.w3-color",
                                {
                                    href: URL.createObjectURL(
                                        new Blob(
                                            [JSON.stringify(state.receipts)],
                                            { type: "application/json;charset=utf-8" })),
                                    download: "cash_receipts.json",
                                    target: "_balnk"
                                },
                                "Export JSON data"
                            ],
                            NBSP,
                            "Import JSON data: ",
                            ["input", {
                                type: "file",
                                accept: "application/json,.json",
                                multiple: false,
                                on: ["change", CashReceiptsActions.import]
                            }]
                        ]]
                    ]]
            ]]
        ];
    },

    dispatcher: async function (this, action, state, dispatch) {
        const storageKey = this.name;

        switch (action.type) {
            case HAppActions.mount:
                state.receipts = JSON.parse(localStorage.getItem(storageKey) || "[]");
                break;

            case HAppActions.umount:
                this.scanner?.clear();
                break;

            case CashReceiptsActions.scan:
                state.qrcode = "";
                state.data = "";
                state.scan = true;
                function onScanSuccess(decodedText, decodedResult) {
                    console.log(`Scan result: ${decodedText}`, decodedResult);
                    dispatch(CashReceiptsActions.scaned, decodedText)
                    // scanner.stop()
                    scanner.clear();
                }
                function onScanError(errorMessage) {
                    // console.error(errorMessage);
                }
                const scanner = new Html5QrcodeScanner(
                    "qr-scaner",
                    {
                        fps: 10,
                        qrbox: (viewfinderWidth, viewfinderHeight) => {
                            return {
                                width: viewfinderWidth * 0.9,
                                height: viewfinderHeight * 0.9
                            };
                        },
                        formatsToSupport: [Html5QrcodeSupportedFormats.QR_CODE]
                    },
                    false
                );
                scanner.render(onScanSuccess, onScanError);
                // const scanner = new Html5Qrcode(
                //     "qr-scaner",
                //     {
                //         formatsToSupport: [Html5QrcodeSupportedFormats.QR_CODE],
                //         verbose: false
                //     });
                // scanner.start(
                //     { facingMode: "environment"},
                //     {
                //         fps: 10,
                //         qrbox: (viewfinderWidth, viewfinderHeight) => {
                //             return {
                //                 width: viewfinderWidth * 0.9,
                //                 height: viewfinderHeight * 0.9
                //             };
                //         }
                //     },
                //     onScanSuccess,
                //     onScanError);
                this.scanner = scanner;
                break;

            case CashReceiptsActions.scaned:
                state.qrcode = action.data;
                state.scan = false;
                const found = state.receipts.find(r => r.receipt.receiptId === action.data);
                if (/O-.*/.test(state.qrcode)) {
                    let jsonData;
                    if (found) {
                        jsonData = found;
                    } else {
                        const response = await fetch(
                            "https://ekasa.financnasprava.sk/mdu/api/v1/opd/receipt/find",
                            {
                                headers: {
                                    accept: "application/json, text/plain, */*",
                                    // "accept-language":
                                    //     "sk,en-US;q=0.9,en;q=0.8,de;q=0.7",
                                    // "cache-control": "no-cache",
                                    // "sec-ch-ua-mobile": "?0",
                                    "content-type": "application/json;charset=UTF-8",
                                    // ignoreloadingbar: "true",
                                    // pragma: "no-cache",
                                    // "sec-ch-ua":
                                    //     '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
                                    // "sec-ch-ua-platform": '"Linux"',
                                    // "sec-fetch-dest": "empty",
                                    // "sec-fetch-mode": "cors",
                                    // "sec-fetch-site": "same-site",
                                },
                                // referrer: "https://opd.financnasprava.sk/",
                                // referrerPolicy: "strict-origin-when-cross-origin",
                                body: `{"receiptId":"${state.qrcode}"}`,
                                method: "POST",
                                mode: "cors",
                                credentials: "omit",
                            }
                        );
                        jsonData = await response.json();
                        state.receipts.unshift(jsonData)
                        localStorage.setItem(storageKey, JSON.stringify(state.receipts));
                    }
                    state.data = JSON.stringify(jsonData, null, 4);
                }

            case CashReceiptsActions.close:
                state.data = "";
                break;

            case CashReceiptsActions.view:
                state.data = JSON.stringify(state.receipts.find(r => r.receipt.receiptId === action.data), null, 4);
                break;

            case CashReceiptsActions.delete:
                state.receipts = state.receipts.filter(r => r.receipt.receiptId !== action.data)
                localStorage.setItem(storageKey, JSON.stringify(state.receipts));
                break;

            case CashReceiptsActions.import:
                const file = action.data as File;
                state.receipts = await new Response(file).json();
                localStorage.setItem(storageKey, JSON.stringify(state.receipts));
                break;
        }
    }
};

interface Reciept {
    returnValue: number;
    receipt: {
        receiptId: string;
        ico: string;
        cashRegisterCode: string;
        issueDate: string;
        createDate: string;
        customerId: string;
        dic: string;
        icDph: string;
        invoiceNumber: string;
        okp: string;
        paragon: false;
        paragonNumber: string;
        pkp: string;
        receiptNumber: number;
        type: string;
        taxBaseBasic: number;
        taxBaseReduced: number;
        totalPrice: number;
        freeTaxAmount: number;
        vatAmountBasic: number;
        vatAmountReduced: number;
        vatRateBasic: number;
        vatRateReduced: number;
        items: [
            {
                name: string;
                itemType: string;
                quantity: number;
                vatRate: number;
                price: number;
            }
        ];
        organization: {
            buildingNumber: string;
            country: string;
            dic: string;
            icDph: string;
            ico: string;
            municipality: string;
            name: string;
            postalCode: string;
            propertyRegistrationNumber: string;
            streetName: string;
            vatPayer: true;
        };
        unit: {
            cashRegisterCode: string;
            buildingNumber: string;
            country: string;
            municipality: string;
            postalCode: string;
            propertyRegistrationNumber: string;
            streetName: string;
            name: string;
            unitType: string;
        };
        exemption: false;
    };
    searchIdentification: {
        createDate: number;
        bucket: number;
        internalReceiptId: string;
        searchUuid: string;
    };
}
