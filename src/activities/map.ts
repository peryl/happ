import L, { icon, Marker } from "leaflet";
import iconRetinaUrl from "leaflet/dist/images/marker-icon-2x.png";
import iconUrl from "leaflet/dist/images/marker-icon.png";
import shadowUrl from "leaflet/dist/images/marker-shadow.png";
import "leaflet/dist/leaflet.css";
import { HAppActions, HAppI } from "peryl/dist/hsml-app";
import { HashUrlDataState } from "..";

// Assign the imported image assets before you do anything with Leaflet.
Marker.prototype.options.icon = icon({
    iconRetinaUrl,
    iconUrl,
    shadowUrl,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41],
});

import "leaflet.markercluster";
import "leaflet.markercluster/dist/MarkerCluster.css";
import "leaflet.markercluster/dist/MarkerCluster.Default.css";

export interface MapState extends HashUrlDataState {
    title: string;
}

export const map: HAppI<MapState, string> = {

    name: "map",

    state: function () {
        return {
            title: "Map"
        }
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["div#map", { skip: true, style: "height: 500px; width:100%" }, [
                    "Loading..."
                ]]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {
            case HAppActions.mount:
                const tiles = L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
                    maxZoom: 18,
                    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Points &copy 2012 LINZ'
                }),
                latlng = L.latLng(-37.89, 175.41);
                const map = L.map("map", {center: latlng, zoom: 13, layers: [tiles]});
                const markers = L.markerClusterGroup({ chunkedLoading: true });
                const addressPoints = [
                    [-37.8839, 175.3745188667, "571"],
                    [-37.8869090667, 175.3657417333, "486"],
                    [-37.8894207167, 175.4015351167, "807"],
                    [-37.8927369333, 175.4087452333, "899"],
                    [-37.90585105, 175.4453463833, "1273"],
                    [-37.9064188833, 175.4441556833, "1258"],
                    [-37.90584715, 175.4463564333, "1279"],
                    [-37.9033391333, 175.4244005667, "1078"],
                    [-37.9061991333, 175.4492620333, "1309"],
                    [-37.9058955167, 175.4445613167, "1261"],
                    [-37.88888045, 175.39146475, "734"],
                    [-37.8950811333, 175.41079175, "928"],
                    [-37.88909235, 175.3922956333, "740"],
                    [-37.8889259667, 175.3938591667, "759"],
                    [-37.8876576333, 175.3859563833, "687"]
                ];
                for (let i = 0; i < addressPoints.length; i++) {
                    const a = addressPoints[i];
                    const title = a[2];
                    const marker = L.marker(L.latLng(a[0], a[1]), { title: title });
                    marker.bindPopup(title);
                    markers.addLayer(marker);
                }
                map.addLayer(markers);
                break;
        }
    }
};
