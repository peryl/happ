import { HAppActions, HAppI, HFormData } from "peryl/dist/hsml-app";
import { passwordRegex, phoneRegex } from "peryl/dist/validators";
import { usersCreate } from "../services/users";
import { _ } from "../translate";
import { userLogin } from "./user-login";

export interface UserRegisterData {
    name: string;
    email: string;
    password: string;
    phone: string;
}

export interface UserRegisterState {
    title: string;
    form: HFormData<UserRegisterData>;
    registerError?: string;
}

export enum UserRegisterActions {
    change = "user-register-change",
    submit = "user-register-submit"
}

export const userRegister: HAppI<UserRegisterState, UserRegisterActions> = {

    name: "user-register",

    state: function () {
        return {
            title: _("User Register"),
            form: {
                data: {
                    name: "",
                    email: "",
                    password: "",
                    phone: ""
                },
                validation: {}
            }
        };
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["form.w3-container",
                    {
                        novalidate: true,
                        on: [
                            ["change", UserRegisterActions.change],
                            ["submit", UserRegisterActions.submit]
                        ]
                    },
                    [
                        ["p", [
                            ["label", [_("Name"),
                                ["input.w3-input", {
                                    type: "text",
                                    autocomplete: "name",
                                    // placeholder: "Full name",
                                    name: "name",
                                    required: true,
                                    minlength: 3,
                                    maxlength: 50,
                                    value: state.form.data.name
                                }]
                            ]],
                            ["div.w3-text-red", state.form.validation.name]
                        ]],
                        ["p", [
                            ["label", [_("Email"),
                                ["input.w3-input", {
                                    type: "email",
                                    autocomplete: "email",
                                    // placeholder: "Email",
                                    name: "email",
                                    minlength: 3,
                                    maxlength: 50,
                                    required: true,
                                    value: state.form.data.email
                                }]
                            ]],
                            ["div.w3-text-red", state.form.validation.email]
                        ]],
                        ["p", [
                            ["label", [_("Password"),
                                ["input.w3-input", {
                                    type: "password",
                                    autocomplete: "new-password",
                                    // placeholder: "New password",
                                    name: "password",
                                    required: true,
                                    minlength: 8,
                                    maxlength: 20,
                                    pattern: passwordRegex.source,
                                    value: state.form.data.password
                                }]
                            ]],
                            ["div.w3-text-gray", _("At least one number, one uppercase, lowercase letter and one special character")],
                            ["div.w3-text-red", state.form.validation.password]
                        ]],
                        ["p", [
                            ["label", [_("Phone"),
                                ["input.w3-input", {
                                    type: "tel",
                                    autocomplete: "tel",
                                    placeholder: "+421 XXX XXX XXX",
                                    name: "phone",
                                    required: true,
                                    pattern: phoneRegex.source,
                                    value: state.form.data.phone
                                }]
                            ]],
                            ["div.w3-text-gray", _("Phone number in format +421 XXX XXX XXX")],
                            ["div.w3-text-red", state.form.validation.phone]
                        ]],
                        ["p.w3-text-red", [state.registerError]],
                        ["p", [
                            ["button.w3-btn.w3-color", _("Register")],
                            " ",
                            ["a.w3-btn.w3-margin-left",
                                { href: `#${userLogin.name}` },
                                _("Cancel")
                            ]
                        ]]
                    ]
                ]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {

            case HAppActions.init:
                break;

            case UserRegisterActions.change:
                console.log(action.data);
                break;

            case UserRegisterActions.submit:
                const form = action.data as HFormData<UserRegisterData>;
                console.log("form", form);
                state.form = form;
                if (form.valid) {
                    usersCreate({
                        ...form.data,
                        id: "",
                        addressStreet: "",
                        addressPostal: "",
                        addressCity: "",
                        addressCountry: ""
                    });
                    location.hash = `#${userLogin.name}`;
                }
                break;
        }
    }
};
