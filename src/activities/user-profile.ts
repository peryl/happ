import { HAppActions, HAppI, HFormData, HFormInputData } from "peryl/dist/hsml-app";
import { phoneRegex } from "peryl/dist/validators";
import { auth } from "../services/auth";
import { User, usersByEmail, usersUpdate } from "../services/users";
import { userPasswordChange } from "./user-password-change";
import { _ } from "../translate";
import { userLogin } from "./user-login";

export type UserProfileData = {
    id: string;
    name: string;
    /**
     * Phone number in format +421 XXX XXX XXX
     */
    email: string;
    phone: string;
    addressStreet: string;
    addressPostal: string;
    addressCity: string;
    addressCountry: string;
};

export interface UserProfileState {
    title: string;
    form: HFormData<UserProfileData>;
    saved: boolean;
}

export enum UserProfileActions {
    input = "user-profile-input",
    change = "user-profile-change",
    submit = "user-profile-submit"
}

export const userProfile: HAppI<UserProfileState, UserProfileActions> = {

    name: "user-profile",

    state: function () {
        return {
            title: _("User Profile"),
            form: {
                data: {
                    id: "",
                    name: "",
                    email: "",
                    phone: "",
                    addressStreet: "",
                    addressPostal: "",
                    addressCity: "",
                    addressCountry: ""
                },
                validation: {}
            },
            saved: true
        };
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["form.w3-container",
                    {
                        novalidate: true,
                        on: [
                            ["input", UserProfileActions.input],
                            ["change", UserProfileActions.change],
                            ["submit", UserProfileActions.submit]
                        ]
                    },
                    [
                        ["p", [
                            ["label", [_("Name"),
                                ["input.w3-input", {
                                    type: "text",
                                    autocomplete: "name",
                                    placeholder: "Full name",
                                    name: "name",
                                    required: true,
                                    minlength: 3,
                                    maxlength: 50,
                                    value: state.form.data.name
                                }]
                            ]],
                            ["div.w3-text-red", state.form.validation.name]
                        ]],
                        ["p", [
                            ["label", [_("Email"),
                                ["span.w3-input", state.form.data.email]
                            ]],
                            ["div.w3-text-red", state.form.validation.email]
                        ]],
                        ["p", [
                            ["label", [_("Phone"),
                                ["input.w3-input", {
                                    type: "tel",
                                    autocomplete: "tel",
                                    placeholder: "+421 XXX XXX XXX",
                                    name: "phone",
                                    required: true,
                                    pattern: phoneRegex.source,
                                    value: state.form.data.phone
                                }]
                            ]],
                            ["div.w3-text-gray", _("Phone number in format +421 XXX XXX XXX")],
                            ["div.w3-text-red", state.form.validation.phone]
                        ]],
                        ["p", [
                            ["label", [_("Address"),
                                ["input.w3-input", {
                                    type: "text",
                                    autocomplete: "street-address",
                                    placeholder: _("Street"),
                                    name: "addressStreet",
                                    required: true,
                                    minlength: 3,
                                    maxlength: 100,
                                    value: state.form.data.addressStreet
                                }],
                                ["div.w3-text-red.w3-margin-bottom", state.form.validation.addressStreet],
                                ["input.w3-input", {
                                    type: "text",
                                    autocomplete: "postal-code",
                                    placeholder: _("Postal code"),
                                    name: "addressPostal",
                                    required: true,
                                    minlength: 3,
                                    maxlength: 10,
                                    value: state.form.data.addressPostal
                                }],
                                ["div.w3-text-red.w3-margin-bottom", state.form.validation.addressPostal],
                                ["input.w3-input", {
                                    type: "text",
                                    autocomplete: "address-level2",
                                    placeholder: _("City"),
                                    name: "addressCity",
                                    required: true,
                                    minlength: 3,
                                    maxlength: 30,
                                    value: state.form.data.addressCity
                                }],
                                ["div.w3-text-red.w3-margin-bottom", state.form.validation.addressCity],
                                ["input.w3-input", {
                                    type: "text",
                                    autocomplete: "country-name",
                                    placeholder: _("Country"),
                                    name: "addressCountry",
                                    required: true,
                                    minlength: 3,
                                    maxlength: 20,
                                    value: state.form.data.addressCountry
                                }],
                                ["div.w3-text-red", state.form.validation.addressCountry],
                            ]]
                        ]],
                        ["p", [
                            ["button.w3-btn.w3-color", { disabled: state.saved }, _("Save")],
                            " ",
                            ["a.w3-btn.w3-margin-left", { href: "#" }, _("Cancel")],
                        ]],
                        ["hr"],
                        ["p.w3-center", [
                            ["a.w3-btn.w3-color.w3-text-white",
                                { href: "#user-login" },
                                _("Logout")
                            ],
                            " ",
                            ["a.w3-btn.w3-dark-gray.w3-text-white.w3-margin-left",
                                { href: `#${userPasswordChange.name}` },
                                _("Password change")
                            ]
                        ]]
                    ]
                ]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        if (!auth.valid()) {
            location.hash = `#${userLogin.name}/${this.name}`;
        }

        switch (action.type) {

            case HAppActions.init:
                break;

            case HAppActions.mount: {
                // http get profile
                const a = auth.get();
                if (a) {
                    const user = usersByEmail(a.payload.sub!);
                    state.form.data = user!;
                }
                break;
            }

            case UserProfileActions.input:
                state.saved = false;
                break;

            case UserProfileActions.change:
                const input = action.data as HFormInputData;
                console.log("change", JSON.stringify(input, null, 4));
                state.form.data[input.name!] = input.value;
                state.form.validation[input.name!] = input.validation;
                break;

            case UserProfileActions.submit: {
                const form = action.data as HFormData<UserProfileData>;
                console.log("form", form);
                state.form = form;
                const a = auth.get();
                if (a) {
                    const user = usersByEmail(a.payload.sub!);
                    if (user) {
                        state.form.data.email = user.email;
                        if (form.valid) {
                            const u: User = {
                                ...state.form.data,
                                id: user.id,
                                email: user.email,
                                password: user.password
                            };
                            usersUpdate(u);
                            state.saved = true;
                        }
                    }
                }
                break;
            }
        }
    }
};
