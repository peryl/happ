import { HElement } from "peryl/dist/hsml";
import { HAppI } from "peryl/dist/hsml-app";

export interface Avatar {
    id: string;
    name: string;
    role: string;
    image: string;
}

export interface AvatarListState {
    title: string;
    avatars: Avatar[];
}

export enum AvatarListAction {
    delete = "avatar-list-delete"
}

export const avatarList: HAppI<AvatarListState, AvatarListAction> = {

    name: "avatar-list",

    state: function () {
        return {
            title: "Avatar List",
            avatars: [
                {
                    id: "1",
                    name: "Mike",
                    role: "Web Designer",
                    image: "https://www.w3schools.com/w3css/img_avatar2.png"
                },
                {
                    id: "2",
                    name: "Jill",
                    role: "Support",
                    image: "https://www.w3schools.com/w3css/img_avatar5.png"
                },
                {
                    id: "3",
                    name: "Jane",
                    role: "Accountant",
                    image: "https://www.w3schools.com/w3css/img_avatar6.png"
                },
                {
                    id: "4",
                    name: "Jack",
                    role: "Advisor",
                    image: "https://www.w3schools.com/w3css/img_avatar3.png"
                }
            ]
        };
    },


    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["ul.w3-ul.w3-card-4.w3-white",
                    state.avatars.map<HElement<AvatarListAction>>(avatar =>
                        ["li.w3-bar", [
                            ["button.w3-bar-item.w3-button.w3-white.w3-large.w3-right",
                                { on: ["click", AvatarListAction.delete, avatar.id] },
                                [["i.fa.fa-trash"]]
                            ],
                            ["img.w3-bar-item.w3-circle.w3-hide-small-",
                                { src: avatar.image, style: "width:85px" }
                            ],
                            ["div.w3-bar-item", [
                                ["span.w3-large", avatar.name],
                                ["br"],
                                ["span", avatar.role]
                            ]]
                        ]]
                    )
                ]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {
            case AvatarListAction.delete:
                state.avatars = state.avatars.filter(a => a.id !== action.data);
                break;
        }
    }
};
