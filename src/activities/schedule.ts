import { HElement } from "peryl/dist/hsml";
import { HAppI } from "peryl/dist/hsml-app";
import { HashUrlDataState } from "..";

// https://www.liquidlight.co.uk/blog/create-an-event-schedule-with-html-table/

interface ScheduleEvent {
    title: string;
    subtitle: string;
    stage: string;
    start: string;
    end: string;
    span: number;
}

const scheduleEvents: ScheduleEvent[] = [
    {
        title: "Arrival and registration",
        subtitle: "Registration Area",
        stage: "stage-saturn",
        start: "8:00",
        end: "9:30",
        span: 4
    },
    {
        title: "Welcome",
        subtitle: "Earth Stage",
        stage: "stage-earth",
        start: "10:00",
        end: "10:00",
        span: 4
    },
    {
        title: "Speaker One",
        subtitle: "Earth Stage",
        stage: "stage-earth",
        start: "10:30",
        end: "10:30",
        span: 4
    },
    {
        title: "Speaker Two",
        subtitle: "Earth Stage",
        stage: "stage-earth",
        start: "11:00",
        end: "11:00",
        span: 4
    },
    {
        title: "Speaker Three",
        subtitle: "Earth Stage",
        stage: "stage-earth",
        start: "11:30",
        end: "11:30",
        span: 4
    },
    {
        title: "Speaker Five",
        subtitle: "Mercury Stage",
        stage: "stage-mercury",
        start: "12:00",
        end: "16:00",
        span: 1
    },
    {
        title: "Speaker Six",
        subtitle: "Venus Stage",
        stage: "stage-venus",
        start: "12:00",
        end: "16:00",
        span: 1
    },
    {
        title: "Speaker Seven",
        subtitle: "Mars Stage",
        stage: "stage-mars",
        start: "12:00",
        end: "16:00",
        span: 1
    },
    {
        title: "Lunch",
        subtitle: "Saturn Stage",
        stage: "stage-saturn",
        start: "12:00",
        end: "13:00",
        span: 1
    },
    {
        title: "Speaker Eigth",
        subtitle: "Earth Stage",
        stage: "stage-earth",
        start: "16:30",
        end: "16:30",
        span: 4
    },
    {
        title: "Speaker Nine",
        subtitle: "Earth Stage",
        stage: "stage-earth",
        start: "17:00",
        end: "17:00",
        span: 4
    },
    {
        title: "Break",
        subtitle: "Saturn Stage",
        stage: "stage-saturn",
        start: "17:30",
        end: "17:30",
        span: 4
    },
    {
        title: "Speaker Ten",
        subtitle: "Earth Stage",
        stage: "stage-earth",
        start: "18:00",
        end: "19:00",
        span: 1
    },
    {
        title: "Speaker Eleven",
        subtitle: "Mercury Stage",
        stage: "stage-mercury",
        start: "18:00",
        end: "19:00",
        span: 1
    },
    {
        title: "Speaker Twelve",
        subtitle: "Jupiter Stage",
        stage: "stage-jupiter",
        start: "18:00",
        end: "19:00",
        span: 2
    },
    {
        title: "Speaker Thirteen",
        subtitle: "Venus Stage",
        stage: "stage-venus",
        start: "19:30",
        end: "21:30",
        span: 2
    },
    {
        title: "Drinks",
        subtitle: "Saturn Stage",
        stage: "stage-saturn",
        start: "19:30",
        end: "21:30",
        span: 2
    }
];

export interface ScheduleState extends HashUrlDataState {
    title: string;
    schedule: ScheduleEvent[];
}

export enum ScheduleAction {
    select = "schedule-select"
}

export const schedule: HAppI<ScheduleState, ScheduleAction> = {

    name: "schedule",

    state: function () {
        return {
            title: "Schedule",
            schedule: scheduleEvents
        };
    },

    view: function (state) {
        const blockTimesFromHour = 8;
        const blockTimesToHour = 21;
        const blockTimes: string[] = [];
        for (let i = blockTimesFromHour; i <= blockTimesToHour; i++) {
            blockTimes.push(`${i}:00`);
            blockTimes.push(`${i}:30`);
        }
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["div.w3-responsive", [
                    ["div.schedule", [
                        ["div.schedule-container", [
                            // Times
                            ...blockTimes.map<HElement<ScheduleAction>>(t =>
                                [`div.schedule-time.start-${t.replace(/:/g, '')}`, t]),
                            // Events
                            ...state.schedule.map<HElement<ScheduleAction>>(ev =>
                                ["div.schedule-event",
                                    {
                                        classes: [
                                            ev.stage,
                                            `start-${ev.start.replace(/:/g, '')}`,
                                            `end-${ev.end.replace(/:/g, '')}`,
                                            `span-${ev.span}`],
                                        on: ["click", ScheduleAction.select, ev]
                                    },
                                    [ev.title, ["span", ev.subtitle]]
                                ])
                        ]]
                    ]]
                ]]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {
            case ScheduleAction.select:
                console.log("selected:", JSON.stringify(action.data, null, 4));
                break;
        }
    }
};
