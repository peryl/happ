import { HElement, hjoin } from "peryl/dist/hsml";
import { HAppActions, HAppI, HFormInputData } from "peryl/dist/hsml-app";
import { HashUrlDataState } from "..";
import { BookingEvent, bookingsAll, bookingsByIdAndDate, bookingsEventDelete, bookingsEventInsert, bookingsEventUpdate, dateIsoString } from "../services/bookings";
import { bookings } from "./bookings";
import { HFormData } from "peryl/dist/hsml-app";

// https://www.liquidlight.co.uk/blog/create-an-event-schedule-with-html-table/

export interface BookingState extends HashUrlDataState {
    title: string;
    date: string;
    bookingId?: string;
    bookingTitle?: string;
    events: BookingEvent[];
    form?: HFormData<BookingEvent>;
    selectTimeFrom?: string;
}

export enum BookingActions {
    date = "booking-date",
    dateToday = "booking-date-today",
    datePrev = "booking-date-prev",
    dateNext = "booking-date-next",
    load = "booking-load",
    time = "booking-time",
    edit = "booking-edit",
    change = "booking-change",
    color = "booking-color",
    save = "booking-save",
    cancel = "booking-cancel",
    delete = "booking-delete"
}

export const booking: HAppI<BookingState, BookingActions> = {

    name: "booking",

    state: function () {
        return {
            title: "Booking",
            date: dateIsoString(new Date()),
            form: undefined,
            bookingId: undefined,
            bookingTitle: undefined,
            events: []
        };
    },

    view: function (state) {
        const daysAhead = 12;
        const blockTimes: string[][] = times();
        const blockTimesEmpty = timesEmpty(state.events);
        const dateString = new Date(state.date).toDateString();
        const today = new Date();
        return [
            ["div.w3-content", [
                ["h1", [
                    state.bookingTitle ?? state.title,
                    ["a.w3-btn.w3-color.w3-right.w3-large",
                        { href: `#${bookings.name}` },
                        [["i.fa.fa-list"]]
                    ]
                ]],
                state.form
                    ? ["form.w3-container",
                        {
                            novalidate: true,
                            on: [
                                ["submit", BookingActions.save],
                                ["change", BookingActions.change]
                            ]
                        }, [
                        // ["p.w3-small", [
                        //     ["label", ["ID",
                        //         ["span.w3-input", state.event.id]
                        //     ]],
                        //     ["input", { type: "hidden", name: "id", value: state.event.id }]
                        // ]],
                        ["input", { type: "hidden", name: "id", value: state.form.data.id }],
                        ["p", [
                            ["label", ["Title",
                                ["input.w3-input", {
                                    type: "text",
                                    name: "title",
                                    placeholder: "Title",
                                    required: true,
                                    minlength: 3,
                                    maxlength: 30,
                                    value: state.form.data.title
                                }]
                            ]],
                            ["div.w3-text-red", state.form.validation.title]
                        ]],
                        ["p", [
                            ["label", ["From",
                                ["select.w3-select", {
                                        name: "timeFrom",
                                        required: true
                                    },
                                    [
                                        ["option",
                                            {
                                                value: "",
                                                disabled: true,
                                                selected: true
                                            },
                                            "From"
                                        ],
                                        ...blockTimes
                                            .filter(t => toTime(t[0]) >= toTime(state.form!.data.timeFrom))
                                            .filter(t => toTime(t[0]) <= toTime(state.form!.data.timeTo))
                                            .map<HElement<BookingActions>>(time => (
                                                ["option",
                                                    {
                                                        value: time[0],
                                                        selected: time[0] === state.form?.data.timeFrom
                                                    },
                                                    time[0]
                                                ])
                                        )
                                    ]
                                ]
                            ]],
                            ["div.w3-text-red", state.form.validation.timeFrom]
                        ]],
                        ["p", [
                            ["label", ["To",
                                ["select.w3-select", {
                                        name: "timeTo",
                                        required: true
                                    },
                                    [
                                        ["option",
                                            {
                                                value: "",
                                                disabled: true,
                                                selected: true
                                            },
                                            "To"
                                        ],
                                        ...blockTimes
                                            .filter(t => toTime(t[0]) >= toTime(state.selectTimeFrom ?? state.form!.data.timeFrom))
                                            .filter(t => toTime(t[0]) <= toTime(state.form!.data.timeTo))
                                            // .map((t, i) => [blockTimes[i+1])
                                            .map<HElement<BookingActions>>(time => (
                                                ["option",
                                                    {
                                                        key: time[0],
                                                        value: time[0],
                                                        selected: (time[0] === state.form?.data.timeFrom) && !state.selectTimeFrom
                                                    },
                                                    time[1]
                                                ])
                                        )
                                    ]
                                ]
                            ]],
                            ["div.w3-text-red", state.form.validation.timeTo]
                        ]],
                        ["p", [
                            ["label", ["Color ",
                                ["div",
                                    hjoin([
                                            "#ffd505",
                                            "#ffa726",
                                            "#ff8a65",
                                            "#9ccc65",
                                            "#81d4fa",
                                            "#26c6da",
                                            "#d596df",
                                            "#da9b84",
                                            "#ff9999"
                                            // Vivid Colors
                                            // "#ffb5bA",
                                            // "#ffb7a5",
                                            // "#be0032",
                                            // "#e25822",
                                            // "#f38400",
                                            // "#f6a600",
                                            // "#f3c300",
                                            // "#dcd300",
                                            // "#8db600",
                                            // "#27a64c",
                                            // "#008856",
                                            // "#008882",
                                            // "#0085a1",
                                            // "#00a1c2",
                                            // "#ce4676",
                                            // "#9a4eae",
                                            // "#870074",
                                            // "#30267a"
                                        ].map<HElement<BookingActions>>(color => (
                                            ["button.w3-btn",
                                                {
                                                    type: "button",
                                                    styles: {
                                                        marginBottom: "6px",
                                                        backgroundColor: color,
                                                        color: contrastColor(color)
                                                    },
                                                    on: ["click", BookingActions.color, color]
                                                },
                                                ""
                                            ])
                                        ), " "),
                                ],
                                ["input.w3-block", {
                                    type: "color",
                                    name: "color",
                                    value: new String(state.form.data.color || "#ffa500")
                                }]
                                // ["input.w3-block-", {
                                //     type: "color",
                                //     name: "color",
                                //     value: state.event.color
                                // }]
                                // ["select.w3-select", {
                                //         name: "color",
                                //         required: true`,
                                //         styles: { backgroundColor: state.event?.color },
                                //         on: ["input", BookingAction.color]
                                //     },
                                //     [
                                //         ...[
                                //             ["#ffc107", "Yellow"],
                                //             ["#ffa726", "Orange"],
                                //             ["#ff8a65", "Red"],
                                //             ["#9ccc65", "Green"],
                                //             ["#81d4fa", "Blue"],
                                //             ["#26c6da", "Cyan"],
                                //             ["#d596df", "Purple"],
                                //             ["#da9b84", "Brown"],
                                //             ["#ff9999", "Pink"]
                                //         ].map<HElement>(color => (
                                //             ["option",
                                //                 {
                                //                     value: color[0],
                                //                     selected: color[0] === state.event?.color,
                                //                     styles: { backgroundColor: color[0] }
                                //                 },
                                //                 color[1]
                                //             ])
                                //         )
                                //     ]
                                // ]
                            ]],
                            ["div.w3-text-red", state.form.validation.color]
                        ]],
                        ["p", [
                            ["button.w3-btn.w3-color.w3-padding.w3-right-",
                                { accesskey: "a", title: "alt-a" },
                                "Save"
                            ],
                            " ",
                            ["button.w3-btn.w3-gray.w3-text-white.w3-margin-left",
                                {
                                    type: "button",
                                    accesskey: "c",
                                    title: "alt-c",
                                    on: ["click", BookingActions.cancel]
                                },
                                "Cancel"
                            ]
                        ]]
                    ]]
                    : ["div", [
                        ["div.w3-center", [
                            ["button.w3-button.w3-text-gray.w3-large",
                                {
                                    disabled: new Date(state.date) <= dateAddDays(today, 0),
                                    on: ["click", BookingActions.datePrev],
                                    title: "Previous"
                                },
                                "❮"
                            ],
                            ["button.w3-button.w3-xlarge",
                                {
                                    key: today.toISOString(),
                                    title: "Set today",
                                    styles: { width: `${dateString.length + 0.5}rem` },
                                    on: state.date === dateIsoString(today)
                                        ? ["_", BookingActions.dateToday]
                                        : ["click", BookingActions.dateToday]
                                },
                                new Date(state.date).toLocaleDateString(undefined, { dateStyle: "medium" })
                            ],
                            ["button.w3-button.w3-text-gray.w3-large",
                                {
                                    disabled: new Date(state.date) >= dateAddDays(today, daysAhead - 1),
                                    on: ["click", BookingActions.dateNext],
                                    title: "Next"
                                },
                                "❯"
                            ]
                        ]],
                        ["div.w3-responsive", [
                            ["div.w3-center", { style: "white-space:nowrap" },
                                [...Array(daysAhead).keys()].map<HElement<BookingActions>>((x, i) => {
                                    const today = new Date();
                                    const day = dateAddDays(today, x)
                                    return ["button.w3-button",
                                        {
                                            key: dateIsoString(day),
                                            ref: dateIsoString(day),
                                            classes: [
                                                ["w3-text-gray", day.getDay() === 6 || day.getDay() === 0],
                                            ],
                                            style: state.date === dateIsoString(day)
                                                ? "background-color:#ccc;" :
                                                "",
                                            on: ["click", BookingActions.date, dateIsoString(day)]
                                        },
                                        [
                                            [state.date === dateIsoString(day) ? "b" : "span", [
                                                day.toLocaleString(undefined, { weekday: "short" }),
                                                ["br"],
                                                day.getDate().toString().padStart(2, "0")
                                            ]]
                                        ]
                                    ];
                                }),
                            ]
                        ]],
                        ["div.w3-responsive", [
                            ["div.booking", [
                                ["div.booking-container", [
                                    // Times
                                    ...blockTimes.map<HElement<BookingActions>>(time =>
                                        [`div.booking-time.start-${time[0].replace(/:/g, "")}`,
                                            // { on: ["click", BookingAction.time, time] },
                                            time[0]]),
                                    // Events
                                    ...state.events.map<HElement<BookingActions>>(ev =>
                                        ["div.booking-event",
                                            {
                                                key: ev.id,
                                                styles: {
                                                    backgroundColor: ev.color,
                                                    color: contrastColor(ev.color)
                                                },
                                                classes: [
                                                    `start-${ev.timeFrom.replace(/:/g, "")}`,
                                                    `end-${ev.timeTo.replace(/:/g, "")}`,
                                                    `span-1`
                                                ]
                                                // on: ["click", BookingAction.edit, ev]
                                            },
                                            [
                                                ["div", [
                                                    ["a.w3-right",
                                                        {
                                                            href: "javascript:void(0)",
                                                            styles: { color: contrastColor(ev.color, "#555", "#aaa") },
                                                            title: "Delete",
                                                            on: ["click", BookingActions.delete, ev.id]
                                                        },
                                                        [["i.fa.fa-remove"]]
                                                    ],
                                                    ev.title
                                                ]],
                                                ["span", ev.user]
                                            ]
                                        ]),
                                    ...blockTimesEmpty.map<HElement<BookingActions>>(blockTimeEmpty =>
                                        ["div.booking-event",
                                            {
                                                key: blockTimeEmpty.timeFrom + blockTimeEmpty.timeTo,
                                                styles: { backgroundColor: "#f1f1f1" },
                                                classes: [
                                                    `start-${blockTimeEmpty.timeFrom.replace(/:/g, "")}`,
                                                    `end-${blockTimeEmpty.timeTo.replace(/:/g, "")}`,
                                                    `span-1`
                                                ],
                                                // on: ["click", BookingAction.edit, bt]
                                            },
                                            [
                                                ["div.w3-center", [
                                                    ["a.w3-button.w3-circle",
                                                        {
                                                            style: "color:#ccc;",
                                                            on: ["click", BookingActions.edit, blockTimeEmpty]
                                                        },
                                                        [
                                                            ["i.fa.fa-plus"]
                                                        ]
                                                    ]
                                                ]]
                                            ]
                                        ])
                                ]]
                            ]]
                        ]]
                    ]]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {

            case HAppActions.mount:
                state.bookingId = state.hashUrlData?.path[1] ?? bookingsAll()[0].id;
                dispatch(BookingActions.load);
                break;

            case BookingActions.date:
                state.date = action.data;
                this.refs[state.date].scrollIntoViewIfNeeded(false);
                dispatch(BookingActions.load);
                break;

            case BookingActions.dateToday:
                state.date = dateIsoString(new Date());
                this.refs[state.date].scrollIntoViewIfNeeded(false);
                dispatch(BookingActions.load);
                break;

            case BookingActions.datePrev:
                const dp = new Date(state.date);
                state.date = dateIsoString(dateAddDays(dp, -1));
                this.refs[state.date].scrollIntoViewIfNeeded(false);
                dispatch(BookingActions.load);
                break;

            case BookingActions.dateNext:
                const dn = new Date(state.date);
                state.date = dateIsoString(dateAddDays(dn, 1));
                this.refs[state.date].scrollIntoViewIfNeeded(false);
                dispatch(BookingActions.load);
                break;

            case BookingActions.load:
                if (state.bookingId) {
                    const booking = bookingsByIdAndDate(state.bookingId, state.date);
                    if (booking) {
                        state.bookingTitle = booking.title;
                        state.events = booking.events;
                    }
                }
                break;

            case BookingActions.time:
                console.log("time:", JSON.stringify(action.data, null, 4));
                break;

            case BookingActions.edit:
                state.form = {
                    data: action.data,
                    validation: {}
                };
                state.selectTimeFrom = undefined;
                break;

            case BookingActions.color:
                state.form!.data.color = action.data as string;
                break;

            case BookingActions.change:
                const input = action.data as HFormInputData<string>;
                if (input.name === "color") {
                    state.form!.data.color = input.value;
                    state.form!.validation.color = input.validation;
                }
                if (input.name === "timeFrom") {
                    state.selectTimeFrom = input.value;
                }
                break;

            case BookingActions.save: {
                const form = action.data as HFormData<BookingEvent>;
                state.form = form;
                if (form.valid) {
                    if (state.bookingId) {
                        if (form.data.id) {
                            bookingsEventUpdate(state.bookingId, {
                                id: form.data.id,
                                title: form.data.title,
                                user: "Peter Rybar",
                                date: state.date,
                                timeFrom: form.data.timeFrom,
                                timeTo: form.data.timeTo,
                                color: form.data.color
                            });
                        } else {
                            bookingsEventInsert(state.bookingId, {
                                title: form.data.title,
                                user: "Peter Rybar",
                                date: state.date,
                                timeFrom: form.data.timeFrom,
                                timeTo: form.data.timeTo,
                                color: form.data.color
                            });
                        }
                    }
                    state.form = undefined;
                    dispatch(BookingActions.load);
                }
                break;
            }

            case BookingActions.cancel:
                state.form = undefined;
                break;

            case BookingActions.delete: {
                if (state.bookingId) {
                    bookingsEventDelete(state.bookingId, action.data)
                }
                dispatch(BookingActions.load);
                break;
            }
        }
    }
};

function dateAddDays(date: Date, days: number) {
    var d = new Date(dateIsoString(new Date()));
    d.setTime(new Date(dateIsoString(date)).getTime() + (24 * 60 * 60 * 1000 * days));
    return d;
}

function times() {
    const blockTimesFromHour = 8;
    const blockTimesToHour = 21;
    const blockTimes: string[][] = [];
    for (let i = blockTimesFromHour; i <= blockTimesToHour; i++) {
        blockTimes.push([`${i}:00`, `${i}:30`]);
        blockTimes.push([`${i}:30`, `${i+1}:00`]);
    }
    return blockTimes;
}

function toTime(timeStr: string): number {
    return new Date(`1970-01-01 ${timeStr}`).getTime();
}

function timesEmpty(bookingEvents: BookingEvent[]) {
    return bookingEvents
        .reduce(
            (times, be) => times.filter(s =>
                toTime(s[0]) < toTime(be.timeFrom) ||
                toTime(s[0]) > toTime(be.timeTo)
            ),
            times())
        .map<BookingEvent>(time => ({
            id: undefined,
            title: "",
            user: "",
            date: "",
            timeFrom: time[0],
            timeTo: time[0],
            color: ""
        }))
        .reduce(
            (a, c) => {
                const found = a.find(
                    ai => toTime(ai.timeTo) + 60 * 30 * 1e3 === toTime(c.timeFrom));
                if (found) {
                    found.timeTo = c.timeTo;
                } else {
                    // a.push({ timeFrom: c.timeFrom, timeTo: c.timeTo });
                    a.push(c);
                }
                return a;
            },
            [] as { timeFrom: string; timeTo: string; }[]);
}

/*!
 * Get the contrasting color for any hex color
 * (c) 2021 Chris Ferdinandi, MIT License, https://gomakethings.com
 * Derived from work by Brian Suda, https://24ways.org/2010/calculating-color-contrast/
 * @param  {String} A hexcolor value
 * @return {String} The contrasting color (black or white)
 */
function contrastColor(hexcolor: string, dark="black", light="white") : string {
    if (hexcolor.slice(0, 1) === "#") {
        hexcolor = hexcolor.slice(1);
    }
    if (hexcolor.length === 3) {
        hexcolor = hexcolor
            .split("")
            .map(hex => hex + hex)
            .join("");
    }
    let r = parseInt(hexcolor.substring(0, 0 + 2), 16);
    let g = parseInt(hexcolor.substring(2, 2 + 2), 16);
    let b = parseInt(hexcolor.substring(4, 4 + 2), 16);
    // Get YIQ ratio
    let yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
    return (yiq >= 128) ? dark : light;
}
