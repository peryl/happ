import { HAppActions, HAppI } from "peryl/dist/hsml-app";
import { HashUrlDataState } from "..";
import Chart from "chart.js/auto";

export interface ChartsState extends HashUrlDataState {
    title: string;
}

export const charts: HAppI<ChartsState, string> = {

    name: "charts",

    state: function () {
        return {
            title: "Charts"
        }
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["div.w3-row.w3-section", [
                    ["div.w3-half.w3-padding", [
                        ["div.w3-card.w3-white.w3-padding", [
                            ["h3", "Doughnut"],
                            ["canvas~chart-doughnut", { skip: true }]
                        ]]
                    ]],
                    ["div.w3-half.w3-padding", [
                        ["div.w3-card.w3-white.w3-padding", [
                            ["h3", "Polar Area"],
                            ["canvas~chart-polarArea", { skip: true }]
                        ]]
                    ]]
                ]],
                ["div.w3-row.w3-section", [
                    ["div.w3-half.w3-padding", [
                        ["div.w3-card.w3-white.w3-padding", [
                            ["h3", "Bar"],
                            ["canvas~chart-bar", { skip: true }]
                        ]]
                    ]],
                    ["div.w3-half.w3-padding", [
                        ["div.w3-card.w3-white.w3-padding", [
                            ["h3", "Radar"],
                            ["canvas~chart-radar", { skip: true }]
                        ]]
                    ]]
                ]]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {
            case HAppActions.mount:
                new Chart(this.refs["chart-doughnut"], {
                    type: "doughnut",
                    data: {
                        labels: [
                          "Red",
                          "Blue",
                          "Yellow"
                        ],
                        datasets: [{
                          label: "My First Dataset",
                          data: [300, 50, 100],
                          backgroundColor: [
                            "rgb(255, 99, 132)",
                            "rgb(54, 162, 235)",
                            "rgb(255, 205, 86)"
                          ],
                          hoverOffset: 4
                        }]
                    }
                });
                new Chart(this.refs["chart-polarArea"], {
                    type: "polarArea",
                    data: {
                        labels: [
                          "Red",
                          "Green",
                          "Yellow",
                          "Grey",
                          "Blue"
                        ],
                        datasets: [{
                          label: "My First Dataset",
                          data: [11, 16, 7, 3, 14],
                          backgroundColor: [
                            "rgb(255, 99, 132)",
                            "rgb(75, 192, 192)",
                            "rgb(255, 205, 86)",
                            "rgb(201, 203, 207)",
                            "rgb(54, 162, 235)"
                          ]
                        }]
                    }
                });
                new Chart(this.refs["chart-bar"], {
                    type: "bar",
                    data: {
                        labels: [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July"
                        ],
                        datasets: [{
                          label: "My First Dataset",
                          data: [65, 59, 80, 81, 56, 55, 40],
                          backgroundColor: [
                            "rgba(255, 99, 132, 0.2)",
                            "rgba(255, 159, 64, 0.2)",
                            "rgba(255, 205, 86, 0.2)",
                            "rgba(75, 192, 192, 0.2)",
                            "rgba(54, 162, 235, 0.2)",
                            "rgba(153, 102, 255, 0.2)",
                            "rgba(201, 203, 207, 0.2)"
                          ],
                          borderColor: [
                            "rgb(255, 99, 132)",
                            "rgb(255, 159, 64)",
                            "rgb(255, 205, 86)",
                            "rgb(75, 192, 192)",
                            "rgb(54, 162, 235)",
                            "rgb(153, 102, 255)",
                            "rgb(201, 203, 207)"
                          ],
                          borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        },
                        aspectRatio: 1
                    }
                });
                new Chart(this.refs["chart-radar"], {
                    type: "radar",
                    data: {
                        labels: [
                            "Eating",
                            "Drinking",
                            "Sleeping",
                            "Designing",
                            "Coding",
                            "Cycling",
                            "Running"
                        ],
                        datasets: [{
                            label: "My First Dataset",
                            data: [65, 59, 90, 81, 56, 55, 40],
                            fill: true,
                            backgroundColor: "rgba(255, 99, 132, 0.2)",
                            borderColor: "rgb(255, 99, 132)",
                            pointBackgroundColor: "rgb(255, 99, 132)",
                            pointBorderColor: "#fff",
                            pointHoverBackgroundColor: "#fff",
                            pointHoverBorderColor: "rgb(255, 99, 132)"
                        }, {
                            label: "My Second Dataset",
                            data: [28, 48, 40, 19, 96, 27, 100],
                            fill: true,
                            backgroundColor: "rgba(54, 162, 235, 0.2)",
                            borderColor: "rgb(54, 162, 235)",
                            pointBackgroundColor: "rgb(54, 162, 235)",
                            pointBorderColor: "#fff",
                            pointHoverBackgroundColor: "#fff",
                            pointHoverBorderColor: "rgb(54, 162, 235)"
                        }]
                    },
                    options: {
                        elements: {
                            line: {
                                borderWidth: 3
                            }
                        }
                    }
                });
                break;

            default:
                break;
        }
    }
};
