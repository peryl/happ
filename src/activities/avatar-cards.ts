import { HElement } from "peryl/dist/hsml";
import { HAppI } from "peryl/dist/hsml-app";

export interface Avatar {
    id: string;
    name: string;
    role: string;
    image: string;
}

export interface AvatarCardState {
    title: string;
    avatars: Avatar[];
}

export enum AvatarCardAction {
    select = "avatar-card-select"
}

export const avatarCard: HAppI<AvatarCardState, AvatarCardAction> = {

    name: "avatar-cards",

    state: function () {
        return {
            title: "Avatar Cards",
            avatars: [
                {
                    id: "1",
                    name: "Mike",
                    role: "Web Designer",
                    image: "https://www.w3schools.com/w3css/img_avatar2.png"
                },
                {
                    id: "2",
                    name: "Jill",
                    role: "Support",
                    image: "https://www.w3schools.com/w3css/img_avatar5.png"
                },
                {
                    id: "3",
                    name: "Jane",
                    role: "Accountant",
                    image: "https://www.w3schools.com/w3css/img_avatar6.png"
                }
            ]
        };
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["div.w3-row",
                    state.avatars.map<HElement<AvatarCardAction>>(avatar =>
                        ["div.w3-third", { on: ["click", AvatarCardAction.select, avatar.id]}, [
                            ["div.w3-card.w3-white.w3-margin", [
                                ["img", {
                                    src: avatar.image,
                                    alt: "Person",
                                    style: "width:100%"
                                }],
                                ["div.w3-container", [
                                    ["h4", [
                                        ["b", avatar.name]
                                    ]],
                                    ["p", avatar.role]
                                ]]
                            ]]
                        ]])
                ]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {
            case AvatarCardAction.select:
                break;
        }
    }
};
