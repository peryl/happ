import { HAppActions, HAppI, HFormData } from "peryl/dist/hsml-app";
import { passwordRegex } from "peryl/dist/validators";
import { _ } from "../translate";
import { userProfile } from "./user-profile";
import { userLogin } from "./user-login";
import { auth } from "../services/auth";

export interface UserPasswordChangeData {
    password: string;
    passwordNew: string;
}

export interface UserPasswordChangeState  {
    title: string;
    form: HFormData<UserPasswordChangeData>;
    passwordChangeError?: string;
}

export enum UserPasswordChangeActions {
    change = "user-password-change-change",
    submit = "user-password-change-submit"
}

export const userPasswordChange: HAppI<UserPasswordChangeState, UserPasswordChangeActions> = {

    name: "user-password-change",

    state: function () {
        return {
            title: _("User Password Change"),
            form: {
                data: {
                    password: "",
                    passwordNew: ""
                },
                validation: {}
            }
        };
    },


    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["form.w3-container",
                    {
                        novalidate: true,
                        on: [
                            ["change", UserPasswordChangeActions.change],
                            ["submit", UserPasswordChangeActions.submit]
                        ]
                    },
                    [
                        ["p", [
                            ["label", [_("Current password"),
                                ["input.w3-input", {
                                    type: "password",
                                    autocomplete: "current-password",
                                    // placeholder: "Current password",
                                    name: "password",
                                    required: true,
                                    value: state.form.data.password
                                }]
                            ]],
                            ["div.w3-text-red", state.form.validation.password]
                        ]],
                        ["p", [
                            ["label", [_("New password"),
                                ["input.w3-input", {
                                    type: "password",
                                    autocomplete: "new-password",
                                    // placeholder: "New password",
                                    name: "passwordNew",
                                    required: true,
                                    minlength: 8,
                                    maxlength: 20,
                                    pattern: passwordRegex.source,
                                    value: state.form.data.passwordNew
                                }]
                            ]],
                            ["div.w3-text-gray", _("At least one number, one uppercase, lowercase letter and one special character")],
                            ["div.w3-text-red", state.form.validation.passwordNew]
                        ]],
                        ["p.w3-text-red", [state.passwordChangeError]],
                        ["p", [
                            ["button.w3-btn.w3-color", _("Change")],
                            " ",
                            ["a.w3-btn.w3-margin-left",
                                { href: `#${userProfile.name}` },
                                _("Cancel")
                            ]
                        ]]
                    ]
                ]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        if (!auth.valid()) {
            location.hash = `#${userLogin.name}/${this.name}`;
        }

        switch (action.type) {

            case HAppActions.init:
                break;

            case UserPasswordChangeActions.change:
                console.log(action.data);
                break;

            case UserPasswordChangeActions.submit:
                const form = action.data as HFormData<UserPasswordChangeData>;
                console.log("form", form);
                state.form = form;
                if (form.valid) {
                    console.log(action.data);
                    alert(JSON.stringify(action.data, null, 4));
                }
                break;
        }
    }
};
