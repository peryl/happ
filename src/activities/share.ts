import { HApp, HAppActions, HAppI, HDispatchScopes } from "peryl/dist/hsml-app";
import { AppShellActions } from "../app-shell";

export interface ShareState {
    title: string;
    url: string;
}

export enum ShareActions {
    copyToClipboard = "qrcode-copy-to-clipboard",
    share = "qrcode-share",
    message = "qrcode-message"
}

export const share: HAppI<ShareState, ShareActions> = {

    name: "share",

    state: function () {
        return {
            title: "Share",
            url: ""
        }
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["p.w3-center", [
                    ["img", {
                        src: `https://chart.googleapis.com/chart?cht=qr&chs=500&chld=L|1&chl=${encodeURIComponent(state.url)}`,
                        style: "width:100%;max-width:500px",
                        alt: state.url
                    }]
                ]],
                ["p.w3-center", [
                    state.url
                ]],
                ["p.w3-center", [
                    ["button.w3-button.w3-color",
                        { on: ["click", ShareActions.copyToClipboard], title: "Copy to clipboard" },
                        [["i.fa.fa-fw.fa-link"], " ", "Copy to clipboard"]
                    ],
                    " ",
                    !!navigator.share
                        ? ["button.w3-button.w3-color.w3-margin-left",
                            { on: ["click", ShareActions.share], title: "Share" },
                            [["i.fa.fa-fw.fa-share-alt"], " ", "Share"]
                        ]
                        : null
                    ]]
                ]]
        ];
    },

    dispatcher:  async function (this: HApp<ShareActions, AppShellActions>, action, state, dispatch) {
        switch (action.type) {

            case HAppActions.mount:
                state.url = location.href;
                break;

            case ShareActions.copyToClipboard:
                const link = location.href;
                copyToClipboard(link);
                dispatch(ShareActions.message, "Link copied to clipboard", HDispatchScopes.window);
                break;

            case ShareActions.share:
                if (navigator.share) {
                    const urlShare = location.href;
                    try {
                        await navigator.share({
                            title: "Share Title",
                            text: "Share Text",
                            url: urlShare
                        });
                    } catch (error) {
                        console.error(error);
                    }
                } else {
                    console.warn("Share API not supported");
                    dispatch(ShareActions.message, "Share API not supported", HDispatchScopes.window);
                }
                break;
        }
    }
};

function copyToClipboard(text: string) {
    const dummy = document.createElement("input");
    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select(); // browsers
    dummy.setSelectionRange(0, 99999); // mobile devices
    navigator.clipboard.writeText(dummy.value);
    document.body.removeChild(dummy);
}
