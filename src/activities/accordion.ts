import { HAppI } from "peryl/dist/hsml-app";

export interface AccordionState {
    title: string;
    accordion: {
        first: boolean;
        second: boolean;
    };
}

export enum AccordionAction {
    switch = "accordion-switch"
}

export const accordion: HAppI<AccordionState, AccordionAction> = {

    name: "accordion",

    state: function () {
        return {
            title: "Accordion",
            accordion: {
                first: false,
                second:false
            }
        };
    },

    view: function (state) {
        return [
            ["div.w3-content", [
                ["h1", state.title],
                ["button.w3-button.w3-padding.w3-block.w3-left-align.w3-color.w3-card",
                    { on: ["click", AccordionAction.switch, "first"] },
                    [
                        "Accordion first",
                        ["i.fa.fa-caret-down.w3-right"]
                    ]
                ],
                ["div.w3-card.w3-white",
                    { class: state.accordion.first ? "w3-show" : "w3-hide" },
                    [
                        ["div.w3-container", [
                            ["p", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."]
                        ]]
                    ]
                ],
                ["button.w3-button.w3-padding.w3-block.w3-left-align.w3-color.w3-card",
                    { on: ["click", AccordionAction.switch, "second"] },
                    [
                        "Accordion second",
                        ["i.fa.fa-caret-down.w3-right"]
                    ]
                ],
                ["div.w3-card.w3-white",
                    { class: state.accordion.second ? "w3-show" : "w3-hide" },
                    [
                        ["a.w3-button.w3-block.w3-left-align",
                            { href: "javascript:void(0)" },
                            "Link 1"
                        ],
                        ["a.w3-button.w3-block.w3-left-align",
                            { href: "javascript:void(0)" },
                            "Link 2"
                        ],
                        ["a.w3-button.w3-block.w3-left-align",
                            { href: "javascript:void(0)" },
                            "Link 3"
                        ]
                    ]
                ]
            ]]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {
            case AccordionAction.switch:
                state.accordion[action.data] = !state.accordion[action.data];
                break;
        }
    }
};
