import { HElement, NBSP } from "peryl/dist/hsml";
import { HApp, HDispatcher, HState, HView } from "peryl/dist/hsml-app";
import { _, translateInit } from "./translate";
import { messages } from "./translate_apps";

translateInit(navigator.languages, messages);

interface State {
    title: string;
    subtitle: string;
    apps: {
        name: string;
        link: string;
        color: string;
        icon: string;
    }[];
}

enum Actions {
    none = "none"
}

const state: HState<State> = function () {
    return {
        title: "PeRyL HApp",
        subtitle: _("Applications"),
        apps: [
            { name: "Demo", link: "index.html", color: "blue", icon: "paper-plane-o" },
            { name: "Menu", link: "menu.html", color: "green", icon: "file-text-o" },
            { name: "Navigation", link: "navigation.html", color: "orange", icon: "navicon" }
        ]
    }
};

const view: HView<State, Actions> = function (state) {
    return [
        ["div.w3-bar.w3-color.w3-large.w3-card", [
            ["span.w3-bar-item", [
                ["strong", [
                    ["a", { href: "#", style: "text-decoration: none;" },
                        state.title
                    ]
                ]],
                ["span.w3-hide-small", state.subtitle ? " - " : ""],
                ["span.w3-hide-small", state.subtitle ? state.subtitle : ""],
            ]],
            ["a.w3-bar-item.w3-button.w3-right",
                {
                    href: "https://gitlab.com/peryl/happ",
                    title: "GitLab repository",
                    target: "_blank",
                    rel: "noopener"
                },
                [["i.fa.fa-gitlab"]]
            ],
            // ["div.w3-dropdown-hover.w3-right", [
            //     ["button.w3-button", [["i.fa.fa-user"]]],
            //     ["div.w3-dropdown-content.w3-bar-block.w3-card-4", [
            //         ["a.w3-bar-item.w3-button",
            //             { href: "#user-profile" },
            //             "Profile"
            //         ],
            //         ["a.w3-bar-item.w3-button",
            //             { href: "#user-logout" },
            //             "Logout"
            //         ]
            //     ]]
            // ]]
        ]],
        ["div.w3-content.w3-container", [
            ["h1", state.subtitle],
            ["div",
                state.apps.map<HElement<Actions>>(app =>
                    ["a.w3-card.w3-white.w3-margin-right.w3-margin-bottom.w3-padding.w3-hover-shadow.w3-center-",
                        {
                            href: app.link,
                            styles: {
                                textDecoration: "none",
                                float: "left"
                            }
                        },
                        [
                            // ["img.w3-circle ",
                            //     { src: app.image, style: "width:85px;" }
                            // ],
                            ["span.w3-xlarge.w3-block-",
                                { style: "width: 105px;", class: `w3-text-${app.color}` },
                                [["i.fa.fa-fw", { class: `fa-${app.icon}` }]]
                            ],
                            NBSP,
                            ["span.w3-large.w3-block-", app.name]
                        ]
                    ]
                )
            ]
        ]]
    ];
};

const dispatcher: HDispatcher<State, Actions> = async function (action, state, dispatch) {
    console.log("action", action);
    // console.log("state", state);
    // console.log("happ", this);

    // switch (action.type) {

    //     case Actions.none:
    //         state.subtitle = action.data;
    //         break;

    //     default:
    //         break;
    // }
};

(window as any).app = new HApp<State, Actions>(state, view, dispatcher, "app");
