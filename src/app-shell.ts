import { HElement, HTagHead } from "peryl/dist/hsml";
import { HAppI } from "peryl/dist/hsml-app";
import { userProfile } from "./activities/user-profile";

const nbsp = "\u00a0 ";

export interface AppShellState {
    title: string;
    subtitle: string;
    drawer: boolean;
    menu: {
        label: string;
        url: string;
        icon: string;
        active: boolean;
    }[];
    snackbar?: string;
}

export const enum AppShellActions {
    drawer = "app-shell-drawer",
    message = "app-shell-message",
}

const topBarHeight = "43px";

export const appShell: HAppI<AppShellState, AppShellActions> = {

    name: "app-shell",

    state: function () {
        return {
            title: "PeRyL HApp",
            subtitle: "",
            drawer: false,
            menu: [],
            snackbar: undefined
        }
    },

    view: function (state) {
        return [
            // header
            ["div.w3-top.w3-card",
                {
                    styles: {
                        zIndex: "10002",
                        // maxHeight: topBarHeight
                    }
                },
                [
                    ["div.w3-bar.w3-large.w3-color", [
                            ["button.w3-bar-item.w3-button.w3-hide-large",
                                {
                                    accesskey: "m",
                                    title: "Menu",
                                    on: ["click", AppShellActions.drawer, null]
                                },
                                [["i.fa.fa-bars"]],
                            ],
                            ["span.w3-bar-item", [
                                ["strong", [
                                    ["a", { href: "#", style: "text-decoration: none;" },
                                        state.title
                                    ]
                                ]],
                                ["span.w3-hide-small", state.subtitle ? " - " : ""],
                                ["span.w3-hide-small", state.subtitle ? state.subtitle : ""],
                            ]],
                            // ["a.w3-bar-item.w3-button.w3-right",
                            //     {
                            //         href: "https://gitlab.com/peryl/happ",
                            //         title: "GitLab repository",
                            //         target: "_blank",
                            //         rel: "noopener"
                            //     },
                            //     [["i.fa.fa-gitlab"]]
                            // ],
                            ["a.w3-bar-item.w3-button.w3-right",
                                {
                                    href: "apps.html",
                                    accesskey: "a",
                                    title: "Applications"
                                },
                                [["i.fa.fa-th"]],
                            ],
                            ["a.w3-bar-item.w3-button.w3-right",
                                {
                                    href: "#user-profile",
                                    accesskey: "p",
                                    title: userProfile.state().title
                                },
                                [["i.fa.fa-user"]],
                            ]
                            // ["div.w3-dropdown-hover", [
                            //     ["button.w3-button", ["Menu ", ["i.fa.fa-caret-down"]]],
                            //     ["div.w3-dropdown-content.w3-bar-block.w3-card-4", { styles: { zIndex: "10002" } }, [
                            //         ["a.w3-bar-item.w3-button",
                            //             { href: "#" },
                            //             "Main"
                            //         ],
                            //         ["a.w3-bar-item.w3-button",
                            //             { href: "#data-table" },
                            //             "Data Table"
                            //         ],
                            //         ["a.w3-bar-item.w3-button",
                            //             { href: "#data-form" },
                            //             "Data Form"
                            //         ]
                            //     ]]
                            // ]]
                        ]
                    ]
                    // ["div.w3-bar.w3-color", [
                    //     ["a.w3-bar-item.w3-button",
                    //         { href: "#" },
                    //         "Home"
                    //     ],
                    //     ["a.w3-bar-item.w3-button",
                    //         { href: "#avatar-cards" },
                    //         "Avatar Cards"
                    //     ],
                    //     ["div.w3-dropdown-hover", [
                    //         ["button.w3-button", ["Dropdown ", ["i.fa.fa-caret-down"]]],
                    //         ["div.w3-dropdown-content.w3-bar-block.w3-card-4", [
                    //             ["a.w3-bar-item.w3-button",
                    //                 { href: "#" },
                    //                 "Main"
                    //             ],
                    //             ["a.w3-bar-item.w3-button",
                    //                 { href: "#data-table" },
                    //                 "Data Table"
                    //             ],
                    //             ["a.w3-bar-item.w3-button",
                    //                 { href: "#data-form" },
                    //                 "Data Form"
                    //             ]
                    //         ]]
                    //     ]]
                    // ]],
                    // ]],
                ]
            ],
            // sidebar
            ["div#sidebar.w3-sidebar.w3-collapse.w3-card.w3-animate-left",
                {
                    styles: {
                        zIndex: "10001",
                        paddingBottom: topBarHeight,
                        width: "300px",
                        display: state.drawer ? "block" : "none"
                    }
                },
                [
                    ["div.w3-panel", [
                        // ["h2", [state.title]],
                        ["div.w3-bar-block", {},
                            state.menu.map<HElement<AppShellActions>>(m => (
                                ["a.w3-bar-item.w3-button.w3-padding",
                                    {
                                        href: m.url,
                                        classes: [["w3-light-gray", m.active]],
                                        on: ["click", AppShellActions.drawer, false]
                                    },
                                    [
                                        [m.icon as HTagHead<string>], nbsp, m.label
                                    ]
                                ])
                            )
                        ]
                    ]]
                ]
            ],
            // overlay
            ["div#overlay.w3-overlay.w3-hide-large.w3-animate-opacity",
                {
                    styles: {
                        zIndex: "10000",
                        cursor: "pointer",
                        display: state.drawer ? "block" : "none"
                    },
                    title: "close side menu",
                    on: ["click", AppShellActions.drawer, null]
                }
            ],
            // main
            ["div.w3-main",
                {
                    styles: {
                        marginLeft: "300px",
                        marginTop: topBarHeight
                    }
                },
                [
                    ["div#content.w3-container~content", { skip: true },
                        "..."
                    ]
                ]
            ],
            // snackbar
            ["div#snackbar",
                {
                    classes: [["show", !!state.snackbar]],
                    styles: { zIndex: "10003" }
                },
                state.snackbar
            ]
        ];
    },

    dispatcher: async function (action, state, dispatch) {
        switch (action.type) {

            case AppShellActions.drawer:
                state.drawer = action.data === null ? !state.drawer : action.data;
                break;

            case AppShellActions.message:
                state.snackbar = action.data;
                if (action.data) {
                    setTimeout(
                        () => dispatch(AppShellActions.message),
                        5e3
                    );
                }
                break;
        }
    }
};
