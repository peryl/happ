import { Translations } from "./translate";

export const messages: Translations = {
    "Languages": {
        "sk": "Jazyky"
    },
    "Browser": {
        "sk": "Prehliadač"
    },
    "Dates": {
        "sk": "Dátumy"
    },
    "Numbers": {
        "sk": "Čísla"
    },
    "User Login": {
        "sk": "Prihlásenie používateľa"
    },
    "User Profile": {
        "sk": "Používateľský profil"
    },
    "Email": {
        "sk": "Email"
    },
    "Register new user": {
        "sk": "Zaregistrujte nového používateľa"
    },
    "Password": {
        "sk": "Heslo"
    },
    "Forgot password?": {
        "sk": "Zabudli ste heslo?"
    },
    "Login": {
        "sk": "Prihlásiť sa"
    },
    "Cancel": {
        "sk": "Zrušiť"
    },
    "Logout": {
        "sk": "Odhlásiť sa"
    },
    "Name": {
        "sk": "Meno"
    },
    "Phone": {
        "sk": "Telefón"
    },
    "Phone number in format +421 XXX XXX XXX": {
        "sk": "Telefónne číslo vo formáte +421 XXX XXX XXX"
    },
    "Address": {
        "sk": "Adresa"
    },
    "Street": {
        "sk": "Ulica"
    },
    "Postal code": {
        "sk": "PSČ"
    },
    "City": {
        "sk": "Mesto"
    },
    "Country": {
        "sk": "Krajina"
    },
    "Save": {
        "sk": "Uložiť"
    },
    "Password change": {
        "sk": "Zmeniť heslo"
    },
    "User Password Change": {
        "sk": "Zmena hesla používateľa"
    },
    "Current password": {
        "sk": "Súčasné heslo"
    },
    "New password": {
        "sk": "Nové heslo"
    },
    "Change": {
        "sk": "Zmeniť"
    },
    "At least one number, one uppercase, lowercase letter and one special character": {
        "sk": "Aspoň jedno číslo, jedno veľké písmeno, malé písmeno a jeden špeciálny znak"
    }
}
