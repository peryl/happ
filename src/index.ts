import { Hash, HashUrlData, hashUrlDecode, hashUrlEncode } from "peryl/dist/hash";
import { HElements } from "peryl/dist/hsml";
import { HAction, HApp, HAppActions, HAppI, happ } from "peryl/dist/hsml-app";
import { about } from "./activities/about";
import { accordion } from "./activities/accordion";
import { avatarCard } from "./activities/avatar-cards";
import { avatarList } from "./activities/avatar-list";
import { booking } from "./activities/booking";
import { bookings } from "./activities/bookings";
import { cashReceipts } from "./activities/cash-receipts";
import { charts } from "./activities/charts";
import { dataForm } from "./activities/data-form";
import { dataTable } from "./activities/data-table";
import { formValidation } from "./activities/form-validation";
import { languages } from "./activities/languages";
import { map } from "./activities/map";
import { NotificationsActions, notifications } from "./activities/notifications";
import { qrCodeScan } from "./activities/qrcodescan";
import { schedule } from "./activities/schedule";
import { ShareActions, share } from "./activities/share";
import { tabs } from "./activities/tabs";
import { ticTacToe } from "./activities/tictactoe";
import { userLogin } from "./activities/user-login";
import { userLoginByEmail } from "./activities/user-login-by-email";
import { userPasswordChange } from "./activities/user-password-change";
import { userProfile } from "./activities/user-profile";
import { userRegister } from "./activities/user-register";
import { video } from "./activities/video";
import { AppShellActions, AppShellState, appShell } from "./app-shell";
import { AuthData, AuthDataPayload, auth } from "./services/auth";
import { lists } from "./activities/lists";
import { translateInit } from "./translate";
import { messages } from "./translate_index";

translateInit(navigator.languages, messages);

export interface HashUrlDataState {
    hashUrlData?: HashUrlData;
}

const activities: HAppI<any, any>[] = [
    about,
    avatarCard,
    avatarList,
    lists,
    tabs,
    accordion,
    dataTable,
    dataForm,
    formValidation,
    charts,
    map,
    schedule,
    bookings,
    booking,
    share,
    ticTacToe,
    userRegister,
    userLogin,
    userLoginByEmail,
    userPasswordChange,
    userProfile,
    languages,
    notifications,
    cashReceipts,
    qrCodeScan,
    video
];

type Menu = {
    name: string;
    label: string;
    url: string;
    icon: string;
}[];

const menu: Menu = [
    { name: about.name!, label: about.state().title, url: `#${about.name}/Buddy`, icon: "i.fa.fa-fw.fa-info-circle" },
    { name: avatarCard.name!, label: avatarCard.state().title, url: `#${avatarCard.name}`, icon: "i.fa.fa-fw.fa-id-badge" },
    { name: avatarList.name!, label: avatarList.state().title, url: `#${avatarList.name}`, icon: "i.fa.fa-fw.fa-list" },
    { name: lists.name!, label: lists.state().title, url: `#${lists.name}`, icon: "i.fa.fa-fw.fa-list" },
    { name: tabs.name!, label: tabs.state().title, url: `#${tabs.name}`, icon: "i.fa.fa-fw.fa-list-alt" },
    { name: accordion.name!, label: accordion.state().title, url: `#${accordion.name}`, icon: "i.fa.fa-fw.fa-list-alt" },
    { name: dataTable.name!, label: dataTable.state().title, url: `#${dataTable.name}`, icon: "i.fa.fa-fw.fa-table" },
    { name: dataForm.name!, label: dataForm.state().title, url: `#${dataForm.name}`, icon: "i.fa.fa-fw.fa-edit" },
    { name: formValidation.name!, label: formValidation.state().title, url: `#${formValidation.name}`, icon: "i.fa.fa-fw.fa-check" },
    { name: charts.name!, label: charts.state().title, url: `#${charts.name}`, icon: "i.fa.fa-fw.fa-pie-chart" },
    { name: map.name!, label: map.state().title, url: `#${map.name}`, icon: "i.fa.fa-fw.fa-map" },
    { name: schedule.name!, label: schedule.state().title, url: `#${schedule.name}`, icon: "i.fa.fa-fw.fa-calendar" },
    { name: bookings.name!, label: bookings.state().title, url: `#${bookings.name}`, icon: "i.fa.fa-fw.fa-calendar-check-o" },
    { name: booking.name!, label: booking.state().title, url: `#${booking.name}`, icon: "i.fa.fa-fw.fa-calendar-check-o" },
    { name: share.name!, label: share.state().title, url: `#${share.name}`, icon: "i.fa.fa-fw.fa-share-alt" },
    { name: ticTacToe.name!, label: ticTacToe.state().title, url: `#${ticTacToe.name}`, icon: "i.fa.fa-fw.fa-th" },
    { name: userLogin.name!, label: userLogin.state().title, url: `#${userLogin.name}`, icon: "i.fa.fa-fw.fa-user" },
    { name: userProfile.name!, label: userProfile.state().title, url: `#${userProfile.name}`, icon: "i.fa.fa-fw.fa-user" },
    { name: languages.name!, label: languages.state().title, url: `#${languages.name}`, icon: "i.fa.fa-fw.fa-language" },
    { name: notifications.name!, label: notifications.state().title, url: `#${notifications.name}`, icon: "i.fa.fa-fw.fa-comment" },
    { name: cashReceipts.name!, label: cashReceipts.state().title, url: `#${cashReceipts.name}`, icon: "i.fa.fa-fw.fa-shopping-cart" },
    { name: qrCodeScan.name!, label: qrCodeScan.state().title, url: `#${qrCodeScan.name}`, icon: "i.fa.fa-fw.fa-qrcode" },
    { name: video.name!, label: video.state().title, url: `#${video.name}`, icon: "i.fa.fa-fw.fa-camera" }
];

const debug = location.hostname === "localhost";

const authKey = "auth";
auth.init({
    save: function (auth?: AuthData | undefined): void {
        if (auth) {
            localStorage.setItem(authKey, JSON.stringify(auth));
        } else {
            localStorage.removeItem(authKey);
        }
    },
    load: function (): AuthData | undefined {
        const data = localStorage.getItem(authKey) ?? undefined;
        if (!!data) {
            return JSON.parse(data);
        }
        return;
    },
    decode: function (jwt: string): AuthDataPayload | undefined {
        return JSON.parse(jwt);
    },
    renew: async function (jwt: string, exp: number): Promise<string> {
        const payload = JSON.parse(jwt);
        return JSON.stringify({
            ... payload,
            exp: Math.floor(new Date().getTime() / 1e3) + exp,
            ren: (payload.ren ?? 0) + 1
        });
    },
    renewTimeout: 2,
    renewExp: 20
});
auth.debug = debug;

interface AppState extends AppShellState, HashUrlDataState {
}

enum AppActions {
    activity = "activity",
    hashWrite = "hash-write"
}

window["app"] = happ<AppState, AppShellActions | AppActions>({

    state: function () {
        return appShell.state();
    },

    view: function (state) {
        return appShell.view(state) as any as HElements<any>;
    },

    dispatcher: async function (action, state, dispatch) {
        // console.log("action", action);
        // console.log("state", state);
        // console.log("happ", this);

        switch (action.type) {

            case HAppActions.mount:
                this.hash = new Hash<HashUrlData>()
                    .coders(hashUrlEncode, hashUrlDecode)
                    .onChange(hashUrlData => {
                        // console.log("hash", JSON.stringify(data));
                        state.hashUrlData = hashUrlData;
                        const activityName = (hashUrlData && hashUrlData.path[0]) || activities[0].name;
                        dispatch(AppActions.activity, activityName);
                    })
                    .listen();
                // setTimeout(() => dispatch(AppShellActions.message, "Init message"), 1e3);
                (this as HApp<any, any>).windowActionListen();
                break;

            case AppActions.activity:
                const activityName = action.data as string;
                const activity = activities.find(a => a.name === activityName) || activities[0];
                const app = happ<any, any>({
                    ...activity as HAppI<any, string>,
                    element: this.refs["content"],
                    debug: debug
                });
                window["app_content"] = app;
                app.state.hashUrlData = state.hashUrlData;
                state.subtitle = activity.state().title ?? "";
                state.menu = menu.map(m => ({
                    label: m.label,
                    url: m.url,
                    icon: m.icon,
                    active: m.name === activity.name
                }));
                break;

            case HAppActions.action:
                const a = action.data as HAction<string>;
                switch (a.type) {
                    case ShareActions.message:
                    case NotificationsActions.message:
                        dispatch(AppShellActions.message, a.data);
                        break;
                    default:
                        console.warn("unhandled action", a);
                        break;
                }
                break;

            case AppActions.hashWrite:
                (this.hash as Hash<HashUrlData>).write(hashUrlDecode(action.data));
                break;
        }

        appShell.dispatcher.call(this, action, state, dispatch);
    },

    element: "app",
    debug: location.hostname === "localhost",
    name: appShell.name
});

navigator.serviceWorker
    .register(
        new URL("service-worker.ts", import.meta.url),
        {type: "module"}
    );
    // .then(registration => {
    //     // updateViaCache: "none";
    //     // registration.update();
    //     console.log("Service worker", registration);
    //     if (registration.installing) {
    //         console.log("Service worker installing");
    //     } else if (registration.waiting) {
    //         console.log("Service worker installed");
    //     } else if (registration.active) {
    //         console.log("Service worker active");
    //     }
    //     console.log("Service worker scope", registration.scope);
    // })
    // .catch(error => console.error("Service worker register error", error));
