import { Hash, HashUrlData, hashUrlDecode, hashUrlEncode } from "peryl/dist/hash";
import { HElement, HTagHead, NBSP } from "peryl/dist/hsml";
import { HApp, HAppActions, HAppI, HDispatcher, HState, HView, happ } from "peryl/dist/hsml-app";
import { about } from "./activities/about";
import { avatarCard } from "./activities/avatar-cards";
import { avatarList } from "./activities/avatar-list";
import { userLogin } from "./activities/user-login";
import { userLoginByEmail } from "./activities/user-login-by-email";
import { userPasswordChange } from "./activities/user-password-change";
import { userProfile } from "./activities/user-profile";
import { userRegister } from "./activities/user-register";
import { AuthData, AuthDataPayload, auth } from "./services/auth";

const activities: HAppI<any, any>[] = [
    about,
    avatarCard,
    avatarList,
    userRegister,
    userLogin,
    userLoginByEmail,
    userPasswordChange,
    userProfile
];

type Menu = {
    name: string;
    label: string;
    url: string;
    icon: string;
}[];

const menu: Menu = [
    { name: about.name!, label: about.state().title, url: `#${about.name}`, icon: "i.fa.fa-fw.fa-info-circle" },
    { name: avatarCard.name!, label: avatarCard.state().title, url: `#${avatarCard.name}`, icon: "i.fa.fa-fw.fa-id-badge" },
    { name: avatarList.name!, label: avatarList.state().title, url: `#${avatarList.name}`, icon: "i.fa.fa-fw.fa-list" },
    { name: userProfile.name!, label: userProfile.state().title, url: `#${userProfile.name}`, icon: "i.fa.fa-fw.fa-user"}
];

const debug = location.hostname === "localhost";

const authKey = "auth";
auth.init({
    save: function (auth?: AuthData | undefined): void {
        if (auth) {
            localStorage.setItem(authKey, JSON.stringify(auth));
        } else {
            localStorage.removeItem(authKey);
        }
    },
    load: function (): AuthData | undefined {
        const data = localStorage.getItem(authKey) ?? undefined;
        if (!!data) {
            return JSON.parse(data);
        }
        return;
    },
    decode: function (jwt: string): AuthDataPayload | undefined {
        return JSON.parse(jwt);
    },
    renew: async function (jwt: string, exp: number): Promise<string> {
        const payload = JSON.parse(jwt);
        return JSON.stringify({
            ... payload,
            exp: Math.floor(new Date().getTime() / 1e3) + exp,
            ren: (payload.ren ?? 0) + 1
        });
    },
    renewTimeout: 2,
    renewExp: 20
});
auth.debug = debug;

interface State {
    hashUrlData: HashUrlData;
    title: string;
    subtitle: string;
    menuShow: boolean;
    menu: {
        label: string;
        url: string;
        icon: string;
        active: boolean;
    }[];
}

enum Actions {
    activity = "activity",
    menu = "menu"
}

const state: HState<State> = function () {
    return {
        hashUrlData: {
            path: [],
            query: {}
        },
        title: "PeRyL HApp",
        subtitle: "",
        menu: [],
        menuShow: false
    }
};

const view: HView<State, Actions> = function (state) {
    return [
        ["div.w3-top.w3-card", [
            ["div.w3-bar.w3-color.w3-large", [
                ["div.w3-dropdown-hover"],
                ["div.w3-dropdown-hover", [
                    ["button.w3-button.w3-hide-large.w3-hide-medium",
                        { on: ["click", Actions.menu, null] },
                        [["i.fa.fa-bars"]]
                    ]
                ]],
                ["span.w3-bar-item", [
                    ["strong", [
                        ["a", { href: "#", style: "text-decoration: none;" },
                            state.title
                        ]
                    ]],
                    ["span.w3-hide-small", state.subtitle ? " - " : ""],
                    ["span.w3-hide-small", state.subtitle ? state.subtitle : ""],
                ]],
                // ["a.w3-bar-item.w3-button.w3-hide-small",
                //     { href: "#menu-1" },
                //     "Menu 1"
                // ],
                // ["a.w3-bar-item.w3-button.w3-hide-small",
                //     { href: "#menu-2" },
                //     "Menu 2"
                // ],
                // ["a.w3-bar-item.w3-button.w3-hide-small",
                //     { href: "#menu-3" },
                //     "Menu 3"
                // ],
                // ["a.w3-bar-item.w3-button.w3-right",
                //     {
                //         href: "https://gitlab.com/peryl/happ",
                //         title: "GitLab repository",
                //         target: "_blank",
                //         rel: "noopener"
                //     },
                //     [["i.fa.fa-gitlab"]]
                // ],
                ["a.w3-bar-item.w3-button.w3-right",
                    {
                        href: "apps.html",
                        accesskey: "a",
                        title: "Applications"
                    },
                    [["i.fa.fa-th"]],
                ],
                ["a.w3-bar-item.w3-button.w3-right",
                    {
                        href: "#user-profile",
                        accesskey: "p",
                        title: userProfile.state().title
                    },
                    [["i.fa.fa-user"]],
                ]
            //     ["div.w3-dropdown-hover.w3-right", [
            //         ["button.w3-button", { title: "User" }, [["i.fa.fa-user"]]],
            //         ["div.w3-dropdown-content.w3-bar-block.w3-card-4", [
            //             ["a.w3-bar-item.w3-button",
            //                 { href: "#user-profile" },
            //                 "Profile"
            //             ],
            //             ["a.w3-bar-item.w3-button",
            //                 { href: "#user-logout" },
            //                 "Logout"
            //             ]
            //         ]]
            //     ]]
            ]],
            ["div.w3-bar-block-.w3-color.w3-hide-large-.w3-hide-medium-.menu",
                {
                    styles: {
                        zIndex: "10001",
                        display: state.menuShow ? "block" : "none"
                    }
                },
                state.menu.map<HElement<any>>(m => (
                    ["a.w3-bar-item.w3-mobile.w3-button.w3-left-align",
                        { href: m?.url, on: ["click", Actions.menu, false] },
                        // m?.label
                        [[m?.icon as HTagHead<string>], NBSP, m?.label]
                    ]
                ))
            ]
        ]],
        ["div", [
            ["div.w3-bar.w3-large", [
                ["span.w3-bar-item", NBSP]
            ]],
            ["div.w3-bar.w3-hide-small", [
                ["span.w3-bar-item", NBSP]
            ]]
        ]],
        ["div#content.w3-container~content", { skip: true }, "..."],
        ["div.w3-bar", [
            ["span.w3-bar-item", NBSP]
        ]],
        ["div.w3-bottom.w3-card", [
            ["div.w3-bar.w3-color", [
                ["a.w3-bar-item.w3-button.w3-hover-white",
                    { href: "#menu-1" },
                    "Action 1"
                ],
                ["a.w3-bar-item.w3-button.w3-hover-white",
                    { href: "#menu-2" },
                    "Action 2"
                ],
                ["a.w3-bar-item.w3-button.w3-hover-white",
                    { href: "#menu-3" },
                    "Action 3"
                ]
            ]]
        ]]
    ];
};

const dispatcher: HDispatcher<State, Actions> = async function (action, state, dispatch) {
    console.log("action", action);
    console.log("state", state);
    console.log("happ", this);

    switch (action.type) {

        case HAppActions.mount:
            this.hash = new Hash<HashUrlData>()
                .coders(hashUrlEncode, hashUrlDecode)
                .onChange(hashUrlData => {
                    // console.log("hash", JSON.stringify(data));
                    state.hashUrlData = hashUrlData;
                    const activityName = (hashUrlData && hashUrlData.path[0]) || activities[0].name;
                    dispatch(Actions.activity, activityName);
                })
                .listen();
            // setTimeout(() => dispatch(AppShellActions.message, "Init message"), 1e3);
            (this as HApp<any, any>).windowActionListen();
            break;

        case Actions.activity:
            const activityName = action.data as string;
            const activity = activities.find(a => a.name === activityName) || activities[0];
            const app = happ<any, any>({
                ...activity as HAppI<any, string>,
                element: this.refs["content"],
                debug: debug
            });
            window["app_content"] = app;
            app.state.hashUrlData = state.hashUrlData;
            state.subtitle = app.state.title ?? app.name;
            state.menu = menu.map(m => ({
                label: m.label,
                url: m.url,
                icon: m.icon,
                active: m.name === activityName
            }));
            this.refs["content"].focus();
            break;

        case Actions.menu:
            state.menuShow = action.data === null ? !state.menuShow : action.data;
            break;

        default:
            break;
    }
};

(window as any).app = new HApp<State, Actions>(state, view, dispatcher, "app");
