import { expect } from "chai";
import { it } from "mocha";
import { Auth, AuthConf, AuthData, AuthDataPayload } from "./auth";

describe("auth", function () {

    let data: AuthData | undefined;
    let auth: Auth;

    const nowSecs = Math.floor(new Date().getTime() / 1e3);
    const seconds = nowSecs + 100;
    const payload = { exp: seconds };
    const authData = { jwt: JSON.stringify(payload), payload };

    it("init", async function () {
        const conf: AuthConf = {
            save: function (auth?: AuthData): void {
                data = auth;
                // console.log("save", data);
            },
            load: function (): AuthData | undefined {
                // console.log("load", data);
                return data;
            },
            decode: function (jwt: string): AuthDataPayload | undefined {
                const json = JSON.parse(jwt);
                // console.log("decode", jwt, json);
                return json;
            },
            renew: async function (jwt: string, exp: number): Promise<string> {
                const token = JSON.stringify({ exp: nowSecs + exp });
                // console.log("renew", token);
                return token;
            }
        };
        auth = new Auth();
        // auth.debug = true;
        auth.init(conf);
    });

    it("set", async function () {
        const d = auth.set(authData.jwt);
        expect(d).deep.equal(authData);
    });

    it("get", async function () {
        const res = auth.get();
        expect(res).deep.equal(authData);
    });

    it("renew", async function () {
        const expSecs = 100;
        auth.set(JSON.stringify({ exp: seconds}));
        const validity = await auth.renew(expSecs);
        expect(validity).deep.equal(expSecs);
    });

    it("onChange", async function () {
        auth.onChange(data => {
            // console.log("onChange data", data);
            expect(data).deep.equal(auth.get());
        });
        auth.set(authData.jwt);
        await auth.renew(1);
        auth.set();
    });

});
