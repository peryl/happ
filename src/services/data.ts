export interface Data {
    id?: string;
    name: string;
    born: string;
    children: number;
    married: boolean;
    gender: string;
    sport: string;
}

let datas: Data[] = [];

export function dataAll() {
    return datas;
}

export function dataSelect(id: string) {
    return datas[Number(id)];
}

let id = 0;
export function dataCreate(data: Data) {
    data.id = String(id++);
    datas.push(data);
}

export function dataUpdate(data: Data) {
    datas[Number(data.id)] = data;
}

export function dataDelete(id: string) {
    datas = datas.filter(d => d.id !== id);
}
