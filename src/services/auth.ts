export interface AuthDataPayload {
    iss?: string;
    sub?: string;
    aud?: (string | Array<string>);
    exp?: number;
    nbf?: number;
    iat?: number;
    jti?: string;
    ren?: number;
    name?: string;
    email?: string;
}

export interface AuthData {
    jwt: string;
    payload: AuthDataPayload;
}

export interface AuthConf {
    save: (auth?: AuthData) => void;
    load: () => AuthData | undefined;
    decode: (jwt: string) => AuthDataPayload | undefined;
    renew: (jwt: string, exp: number) => Promise<string>;
    /** Time in seconds before JWT expiraton to renew, default 60 seconds */
    renewTimeout?: number;
    /** Time in seconds renewed JWT will expire, default 60 * 60 * 24 seconds */
    renewExp?: number;
}

export class Auth {

    private _conf: AuthConf;

    debug: boolean = false;

    renewTimeoutDefault: number = 60;
    renewExpDefault: number = 60 * 60 * 24;

    private _timeout: any;
    private _onchange?: (auth?: AuthData) => void;

    constructor(conf?: AuthConf) {
        const _this = this;
        if (!conf) {
            let data: AuthData | undefined;
            this._conf = {
                save: function (auth?: AuthData): void {
                    data = auth;
                },
                load: function (): AuthData | undefined {
                    return data;
                },
                decode: function (jwt: string): AuthDataPayload | undefined {
                    return { exp: _this._nowSeconds() +  _this.renewExpDefault };
                },
                renew: async function (jwt: string, exp: number): Promise<string> {
                    return `jwt-${new Date().toISOString()}`;
                }
            };
        } else {
            this._conf = conf;
        }
        this.debug && console.log("Auth constructor:", this._conf);
    }

    init(conf: AuthConf) {
        this._conf = conf;
        this.debug && console.log("Auth init:", conf);
    }

    set(jwt?: string): AuthData | undefined {
        this._autoRenewStop();
        const authData = !!jwt
            ? { jwt, payload: this._conf.decode(jwt) as any }
            : undefined
        this.debug && console.log("Auth set:", authData);
        this._conf.save(authData);
        authData && this._autoRenewStart();
        this._onchange?.(authData);
        return authData
    }

    get(): AuthData | undefined {
        const authData = this._conf.load();
        this.debug && console.log("Auth get:", authData);
        return authData;
    }

    onChange(cb: (auth?: AuthData) => void) {
        this._onchange = cb;
    }

    validity(): number | undefined {
        const authData = this._conf.load();
        let validity: number | undefined;
        let validityDate: string | undefined;
        if (authData?.payload.exp) {
            validity = authData.payload.exp - this._nowSeconds();
            validityDate = new Date(authData?.payload.exp * 1e3).toISOString();
        }
        this.debug && console.log("Auth validity:", validity, validityDate);
        return validity;
    }

    valid(): boolean {
        const validity = this.validity();
        const valid = !!validity && (validity > 0);
        this.debug && console.log("Auth valid:", valid);
        return valid;
    }

    async renew(renewExp?: number): Promise<number> {
        const auth = this._conf.load();
        let validity = 0;
        if (auth?.payload.exp && auth.jwt) {
            validity = auth.payload.exp - this._nowSeconds();
            this.debug && console.log("Auth renew validity:", validity, new Date(auth.payload.exp * 1e3).toISOString());
            if (validity <= 0) {
                this.debug && console.log("Auth renew token invalid", validity, new Date(auth.payload.exp * 1e3).toISOString());
                this.set();
            } else if (validity > (this._conf.renewTimeout ?? this.renewTimeoutDefault)) {
                this.debug && console.log("Auth renew token valid", validity, new Date(auth.payload.exp * 1e3).toISOString());
            } else {
                this.debug && console.log("Auth renew token renew", validity, new Date(auth.payload.exp * 1e3).toISOString());
                try {
                    const jwt = await this._conf.renew(auth.jwt, renewExp ?? this._conf.renewExp ?? this.renewExpDefault);
                    const payload = this._conf.decode(jwt);
                    if (payload && payload.exp) {
                        const data = { jwt, payload };
                        this._conf.save(data);
                        this._onchange?.(data);
                        validity = payload.exp - this._nowSeconds();
                    } else {
                        this.debug && console.error("Auth renew token renew payload.exp missing", payload);
                    }
                } catch (error) {
                    this.debug && console.error("Auth renew token renew error", error);
                }
            }
        } else {
            this.debug && console.warn("Auth renew token missing");
        }
        return validity;
    }

    private async _autoRenewStart(): Promise<void> {
        this._autoRenewStop();
        const validity = await this.renew();
        if (validity > 0) {
            const seconds = validity
                ? (validity - (this._conf.renewTimeout ?? this.renewTimeoutDefault))
                : (this._conf.renewExp ?? this.renewExpDefault);
            this.debug && console.log("Auth timeout", seconds);
            this._timeout = setTimeout(this._autoRenewStart.bind(this), seconds * 1e3);
        }
    }

    private _autoRenewStop(): void {
        if (this._timeout) {
            clearTimeout(this._timeout);
        }
    }

    private _nowSeconds() {
        return Math.floor(new Date().getTime() / 1e3);
    }

}

export const auth = new Auth();
