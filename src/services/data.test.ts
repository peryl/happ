import { expect } from "chai";
import { it } from "mocha";
import { dataAll, dataCreate, dataDelete } from "./data";

describe("data", function () {

    it("dataAll", function () {
        const data = dataAll();
        expect(data.length).equal(0);
    });

    it("dataCreate", function () {
        dataCreate({
            name: "Ema",
            born: new Date().toString(),
            children: 0,
            married: false,
            gender: "female",
            sport: "gymnastics"
        });
        const data = dataAll();
        expect(data.length).equal(1);
    });

    it("dataDelete", function () {
        dataDelete("0");
        const data = dataAll();
        expect(data.length).equal(0);
    });
});
