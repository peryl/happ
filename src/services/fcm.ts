import { Analytics, getAnalytics } from "firebase/analytics";
import { FirebaseApp, FirebaseOptions, initializeApp } from "firebase/app";
import { getMessaging, getToken, MessagePayload, Messaging, NextFn, Observer, onMessage } from "firebase/messaging";
import { httpPost } from "peryl/dist/http";

export class FCM {

    readonly app: FirebaseApp;
    readonly messaging: Messaging;
    readonly currentToken: string = "";
    readonly analytics: Analytics;

    constructor(firebaseConfig: FirebaseOptions,
        vapidKey: string,
        onInit?: (currentToken: string) => void,
        onData?: NextFn<MessagePayload> | Observer<MessagePayload>
    ) {
        this.app = initializeApp(firebaseConfig);
        this.messaging = getMessaging();
        getToken(this.messaging, { vapidKey })
            .then((currentToken) => {
                if (currentToken) {
                    console.log("FCM device token: " + currentToken);
                    (this.currentToken as any) = currentToken;
                    onInit && onInit(currentToken);
                    // Send the token to your server and update the UI if necessary
                    // ...
                } else {
                    // Show permission request UI
                    console.log("No registration token available. Request permission to generate one.");
                }
            })
            .catch((err) => {
                console.log("An error occurred while retrieving token. ", err);
            });
        onData && onMessage(this.messaging, onData);
        this.analytics = getAnalytics(this.app);
    }

    push(serverKey: string, title: string, body: string, url?: string): void {
        httpPost("https://fcm.googleapis.com/fcm/send")
            .headers({
                "Authorization": `key=${serverKey}`,
                "Content-Type": "application/json"
            })
            .onError(e => console.error("FCM send error", e))
            .onResponse(r => console.log("FCM send response", r.getJson()))
            .send({
                to: this.currentToken,
                notification: {
                    title,
                    body,
                    icon: "https://i.imgur.com/5zO5cce.png",
                    // icon: "https://example.com/image.png" // Optional image
                    // link: url || location.href
                    click_action: url || location.href,
                },
                data: {
                    key: "value"
                }
            });
    }
}
