
export interface Booking {
    id?: string;
    title: string;
    location: string;
    events?: BookingEvent[];
}

export interface BookingEvent {
    id?: string;
    title: string;
    user: string;
    date: string;
    timeFrom: string;
    timeTo: string;
    color: string;
}

export function dateIsoString(date: Date = new Date()): string {
    return date.toISOString().split("T")[0];
}

const key = "bookings";

function load(): Booking[] {
    return JSON.parse(localStorage.getItem(key) || "[]");
}

function save(bookings: Booking[]) {
    localStorage.setItem(key, JSON.stringify(bookings));
}

export function bookingsAll() {
    const bookings = load();
    // Remove old events
    bookings.forEach(b => {
        b.events = b.events!.filter(e => e.date >= dateIsoString(new Date()));
    });
    // Create demo data
    if (!bookings.length) {
        bookings.push({
            id: new Date().toISOString(),
            title: "Meeting room",
            location: "Cowork, Bratislava",
            events: events.map((e, i) => ({
                ...e,
                id: String(i),
                date: dateIsoString(new Date())
            }))
        });
        bookings.push({
            id: new Date().toISOString() + "1",
            title: "Dental clinic",
            location: "Hospotal, Zilina",
            events: events.map((e, i) => ({
                ...e,
                id: String(i),
                date: dateIsoString(new Date())
            }))
        });
        bookings.push({
            id: new Date().toISOString() + "2",
            title: "Laboratory of physics",
            location: "University, Kosice",
            events: events.map((e, i) => ({
                ...e,
                id: String(i),
                date: dateIsoString(new Date())
            }))
        });
    }
    bookings.forEach(b => {
        if (!b.events?.length) {
            b.events = events.map((e, i) => ({
                ...e,
                id: String(i),
                date: dateIsoString(new Date())
            }));
        }
    });
    save(bookings);
    return bookings;
}

export function bookingsById(id: string) {
    const bookings = load();
    return bookings.find(b => b.id === id);
}

export function bookingsByIdAndDate(id: string, date: string) {
    const bookings = load();
    const b = bookings.find(b => b.id === id);
    if (b) {
        return {
            ...b,
            events: b.events!.filter(e => e.date === date)
        }
    } else {
        return undefined;
    }
}

export function bookingsEventsByIdAndDate(bookingId: string, date: string) {
    const bookings = load();
    const b = bookings.find(b => b.id === bookingId);
    if (b) {
        return b.events!.filter(e => e.date === date);
    } else {
        return [];
    }
}

export function bookingsCreate(booking: Booking) {
    booking.id = new Date().toISOString();
    if (!booking.events) {
        booking.events = [];
    }
    const bookings = load();
    bookings.push(booking);
    save(bookings);
}

export function bookingsUpdate(booking: Booking) {
    if (booking.id) {
        const bookings = load();
        const b = bookings.find(b => b.id === booking.id);
        if (b) {
            b.title = booking.title;
            b.location = booking.location;
            save(bookings);
        }
    }
}

export function bookingsDelete(id: string) {
    let bookings = load();
    bookings = bookings.filter(d => d.id !== id);
    save(bookings);
}

export function bookingsEventInsert(bookingId: string, event: BookingEvent) {
    const bookings = load();
    const b = bookings.find(b => b.id === bookingId);
    if (b) {
        b.events?.push({
            ...event,
            id: new Date().toISOString()
        });
        save(bookings);
    }
}

export function bookingsEventUpdate(bookingId: string, event: BookingEvent) {
    const bookings = load();
    const b = bookings.find(b => b.id === bookingId);
    if (b) {
        const e = b.events?.find(e => e.id === event.id);
        if (e) {
            e.title = event.title;
            e.date = event.date;
            e.timeFrom = event.timeFrom;
            e.timeTo = event.timeTo;
            e.color = event.color;
        }
        save(bookings);
    }
}

export function bookingsEventDelete(bookingId: string, eventId: string) {
    const bookings = load();
    const b = bookings.find(b => b.id === bookingId);
    if (b) {
        b.events = b.events!.filter(e => e.id !== eventId);
        save(bookings);
    }
}

const events = [
    {
        title: "PeRyL standup",
        user: "Peter Rybar",
        timeFrom: "9:00",
        timeTo: "9:00",
        color: "#FFA726",
    },
    {
        title: "Steering committee",
        user: "Peter Rybar",
        timeFrom: "10:00",
        timeTo: "11:00",
        color: "#26C6DA",
    },
    {
        title: "Lunch",
        user: "Chef",
        timeFrom: "12:00",
        timeTo: "13:00",
        color: "#9CCC65",
    },
    {
        title: "English lessons",
        user: "Teacher Excelent",
        timeFrom: "14:30",
        timeTo: "16:30",
        color: "#81D4FA",
    },
    {
        title: "Project Planning",
        user: "Product Owner",
        timeFrom: "17:30",
        timeTo: "18:00",
        color: "#FF8A65",
    },
    {
        title: "Networking",
        user: "Peter Rybar",
        timeFrom: "19:30",
        timeTo: "20:30",
        color: "#D596DF",
    },
];
