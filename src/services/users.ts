export interface User {
    id: string;
    name: string;
    /**
     * Phone number in format +421 XXX XXX XXX
     */
    email: string;
    password: string;
    phone: string;
    addressStreet: string;
    addressPostal: string;
    addressCity: string;
    addressCountry: string;
}

const key = "users";

function load(): User[] {
    return JSON.parse(localStorage.getItem(key) || "[]");
}

function save(users: User[]) {
    localStorage.setItem(key, JSON.stringify(users));
}

export function usersAll() {
    const users = load();
    return users;
}

export function usersById(id: string) {
    const users = load();
    return users.find(u => u.id === id);
}

export function usersByEmail(email: string) {
    const users = load();
    return users.find(u => u.email === email);
}

export function usersCreate(user: User) {
    user.id = new Date().toISOString();
    const users = load();
    users.push(user);
    save(users);
}

export function usersUpdate(user: User) {
    if (user.id) {
        const users = load();
        const u = users.find(b => b.id === user.id);
        if (u) {
            u.name = user.name;
            u.phone = user.phone;
            u.addressStreet = user.addressStreet;
            u.addressPostal = user.addressPostal;
            u.addressCity = user.addressCity;
            u.addressCountry = user.addressCountry;
            save(users);
        }
    }
}

export function usersDelete(id: string) {
    let users = load();
    users = users.filter(d => d.id !== id);
    save(users);
}
