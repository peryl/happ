#! /bin/bash

curl -X POST \
  https://api.pushy.me/push?api_key=api_key \
  -H 'Content-Type: application/json' \
  -d '{
    "to" : "e6d9ff93f8e114c4aa4dbe",
    "data": {
        "title": "Test title",
        "message": "Test message",
        "url": "https://gitlab.com/peryl/happ"
    },
    "notification": {
        "body": "Test body",
        "badge": 1,
        "sound": "ping.aiff"
    }
}'
