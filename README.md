# HApp PWA skeleton

[PeRyL](https://gitlab.com/peryl/peryl)
[HApp (HSML App)](https://gitlab.com/peryl/peryl/-/blob/master/HApp.md) project skeleton

Install dependences

```sh
npm install
```

Run development mode

```sh
npm start
```

Create production distribution

```sh
npm run dist
```
